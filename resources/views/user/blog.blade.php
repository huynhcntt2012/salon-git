<!DOCTYPE html>
<html lang="en">
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link href="public/css/bootstrap.min.css" rel="stylesheet">
	<link href="public/css/index.css" rel="stylesheet">
	<link href="public/css/blog.css" rel="stylesheet">
	<link href="public/css/footer.css" rel="stylesheet">
	<link href="public/css/full-slider.css" rel="stylesheet">
	
	<link rel="stylesheet" href="resources/assets/nivoslider/themes/custom/custom.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resources/assets/nivoslider/nivo-slider.css" type="text/css" media="screen" />
	<link href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
</head>
<body>
	<div id="page" class="site">
		<header id="masthead" class="site-header centered container" role="banner">
			<div class="top-panel"> 
				<div class="top-panel__wrap container">
					<div class="top-panel__message">
						<div class="info-block">
							<i class="glyphicon glyphicon-earphone"></i> Call us today: <a href="tel:1-555-644-5566">1-555-644-5566</a>
						</div>
						<div class="info-block">
							<i class="glyphicon glyphicon-map-marker"></i> 7087 Richmond hwy, Alexandria, VA
						</div>
					</div>
					<div class="top-panel__search">
						<form role="search" method="get" class="search-form" action="">
							<label>
							<span class="screen-reader-text">Search for:</span>
							<input type="search" class="search-form__field" placeholder="Search …" value="" name="s" title="Search for:">
							</label>
							<button type="submit" class="search-form__submit btn btn-primary"><i class="material-icons glyphicon glyphicon-search"></i></button>
						</form>
					</div>
				</div>
			</div>
			<div class="header-container">
				<div class="header-container_wrap container">
					<div class="header-container__flex">
						<div class="header-container__center">
							<div class="site-branding">
								<div class="site-logo">
									<a class="site-logo__link" href="https://ld-wp.template-help.com/wordpress_58991/" rel="home">Durand</a>
								</div>
							</div>
							<nav id="site-navigation" class="main-navigation" role="navigation">
								<button class="menu-toggle" aria-controls="main-menu" aria-expanded="false">
								<i class="menu-toggle__icon fa fa-bars" aria-hidden="true"></i>
								Menu </button>
								<ul id="main-menu" class="menu">
                                    <li id="menu-item-1163" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1163"><a href="home">Home</a>
								
								<span class="sub-menu-toggle"></span></li>
								<li id="menu-item-414" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-414">
                                <a href="about">about us</a>
								
								<span class="sub-menu-toggle"></span></li>
								<li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="gallery">Gallery</a>
                                    <ul class="sub-menu">
        								<li id="menu-item-755" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-755"><a href="gallery/image">Image</a></li>
        								<li id="menu-item-756" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-756"><a href="gallery/video">Video</a></li>
								    </ul>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="services">Services</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="blog">blog</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="contact">Contact</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="login">Login</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
								</ul> 
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div id="content" class="site-content container">
			<div class="breadcrumbs"><div class="container"><div class="breadcrumbs__title"></div><div class="breadcrumbs__items">
			<div class="breadcrumbs__content">
			<div class="breadcrumbs__wrap"><div class="breadcrumbs__item"><a href="https://ld-wp.template-help.com/wordpress_58991/" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
			<div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div> <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">News</span></div>
			</div>
			</div></div><div class="clear"></div></div>
			</div>
			<div class="site-content_wrap container">
			<div class="row">
			<div id="primary" class="col-md-12 col-lg-9">
			<main id="main" class="site-main" role="main">
			<header>
			<h1 class="page-title screen-reader-text">News</h1>
			</header>
			<div class="posts-list posts-list--default one-right-sidebar no-sidebars-before">
			<article id="post-206" class="posts-list__item card post-thumbnail--fullwidth post-206 post type-post status-publish format-standard has-post-thumbnail hentry category-tips tag-celebrities tag-events has-thumb">
			<div class="post-list__item-content">
			<figure class="post-thumbnail">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/05/03/how-to-wash-and-style-curly-hair/" class="post-thumbnail__link"><img class="post-thumbnail__img wp-post-image" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image15-1280x510.jpg" alt="How to wash and style curly hair" width="1280" height="510"></a>
			<div class="post__cats"><a href="https://ld-wp.template-help.com/wordpress_58991/category/tips/" rel="tag">Tips</a></div>
			</figure> 
			<header class="entry-header">
			<h4 class="entry-title"><a href="https://ld-wp.template-help.com/wordpress_58991/2016/05/03/how-to-wash-and-style-curly-hair/" rel="bookmark">How to wash and style curly hair</a></h4> </header> 
			<div class="entry-meta">
			<span class="post__date">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/05/03/" class="post__date-link"><time datetime="2016-05-03T12:01:09+00:00" title="2016-05-03T12:01:09+00:00">May 3</time></a> </span>
			<span class="posted-by">by <a href="https://ld-wp.template-help.com/wordpress_58991/author/admin/" class="posted-by__author" rel="author">admin</a></span>
			<span class="post__comments">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/05/03/how-to-wash-and-style-curly-hair/#comments" class="post__comments-link">comments (2)</a> </span>
			</div> 
			<div class="entry-content">
			<p>We finally produced a video guide on how to do the right styling for your curly hair!</p>
			</div> 
			</div> 
			<footer class="entry-footer">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/05/03/how-to-wash-and-style-curly-hair/" class="btn btn-primary"><span class="btn__text">Read more</span><i class="glyphicon glyphicon-arrow-right"></i></a> </footer> 
			</article> 
			<article id="post-204" class="posts-list__item card post-thumbnail--fullwidth post-204 post type-post status-publish format-standard has-post-thumbnail hentry category-celebrities tag-colors-in-trend has-thumb">
			<div class="post-list__item-content">
			<figure class="post-thumbnail">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/24/returning-to-a-natural-hair-texture/" class="post-thumbnail__link"><img class="post-thumbnail__img wp-post-image" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/04/image16-1280x510.jpg" alt="Returning to a natural hair texture" width="1280" height="510"></a>
			<div class="post__cats"><a href="https://ld-wp.template-help.com/wordpress_58991/category/celebrities/" rel="tag">Celebrities</a></div>
			</figure> 
			<header class="entry-header">
			<h4 class="entry-title"><a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/24/returning-to-a-natural-hair-texture/" rel="bookmark">Returning to a natural hair texture</a></h4> </header> 
			<div class="entry-meta">
			<span class="post__date">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/24/" class="post__date-link"><time datetime="2016-04-24T11:49:26+00:00" title="2016-04-24T11:49:26+00:00">April 24</time></a> </span>
			<span class="posted-by">by <a href="https://ld-wp.template-help.com/wordpress_58991/author/admin/" class="posted-by__author" rel="author">admin</a></span>
			<span class="post__comments">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/24/returning-to-a-natural-hair-texture/#respond" class="post__comments-link">comments (0)</a> </span>
			</div> 
			<div class="entry-content">
			<p>In recent years, an increasingly big number of African American women take a fashion decision to return to their natural…</p>
			</div> 
			</div> 
			<footer class="entry-footer">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/24/returning-to-a-natural-hair-texture/" class="btn btn-primary"><span class="btn__text">Read more</span><i class="glyphicon glyphicon-arrow-right"></i></a> </footer> 
			</article> 
			<article id="post-202" class="posts-list__item card post-thumbnail--fullwidth post-202 post type-post status-publish format-standard has-post-thumbnail hentry category-events tag-events tag-haircuts has-thumb">
			<div class="post-list__item-content">
			<figure class="post-thumbnail">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/21/winterish-version-of-rainbow-hair/" class="post-thumbnail__link"><img class="post-thumbnail__img wp-post-image" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/04/image17-1280x510.jpg" alt="Winterish version of rainbow hair?" width="1280" height="510"></a>
			<div class="post__cats"><a href="https://ld-wp.template-help.com/wordpress_58991/category/events/" rel="tag">Events</a></div>
			</figure> 
			<header class="entry-header">
			<h4 class="entry-title"><a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/21/winterish-version-of-rainbow-hair/" rel="bookmark">Winterish version of rainbow hair?</a></h4> </header> 
			<div class="entry-meta">
			<span class="post__date">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/21/" class="post__date-link"><time datetime="2016-04-21T11:28:45+00:00" title="2016-04-21T11:28:45+00:00">April 21</time></a> </span>
			<span class="posted-by">by <a href="https://ld-wp.template-help.com/wordpress_58991/author/admin/" class="posted-by__author" rel="author">admin</a></span>
			<span class="post__comments">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/21/winterish-version-of-rainbow-hair/#respond" class="post__comments-link">comments (0)</a> </span>
			</div> 
			<div class="entry-content">
			<p>Though the winter holidays season may have ended, we’re still eagerly holding on to our love of glitter.</p>
			</div> 
			</div> 
			<footer class="entry-footer">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/21/winterish-version-of-rainbow-hair/" class="btn btn-primary"><span class="btn__text">Read more</span><i class="glyphicon glyphicon-arrow-right"></i></a> </footer> 
			</article> 
			<article id="post-200" class="posts-list__item card post-thumbnail--fullwidth post-200 post type-post status-publish format-standard has-post-thumbnail hentry category-colors-in-trend tag-events tag-haircuts has-thumb">
			<div class="post-list__item-content">
			<figure class="post-thumbnail">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/17/see-these-incredible-photos-of-the-summer-trip-hairstyles/" class="post-thumbnail__link"><img class="post-thumbnail__img wp-post-image" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/04/image19-1280x510.jpg" alt="See these incredible photos of the summer trip hairstyles" width="1280" height="510"></a>
			<div class="post__cats"><a href="https://ld-wp.template-help.com/wordpress_58991/category/colors-in-trend/" rel="tag">Colors in Trend</a></div>
			</figure> 
			<header class="entry-header">
			<h4 class="entry-title"><a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/17/see-these-incredible-photos-of-the-summer-trip-hairstyles/" rel="bookmark">See these incredible photos of the summer trip hairstyles</a></h4> </header> 
			<div class="entry-meta">
			<span class="post__date">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/17/" class="post__date-link"><time datetime="2016-04-17T11:27:54+00:00" title="2016-04-17T11:27:54+00:00">April 17</time></a> </span>
			<span class="posted-by">by <a href="https://ld-wp.template-help.com/wordpress_58991/author/admin/" class="posted-by__author" rel="author">admin</a></span>
			<span class="post__comments">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/17/see-these-incredible-photos-of-the-summer-trip-hairstyles/#respond" class="post__comments-link">comments (0)</a> </span>
			</div> 
			<div class="entry-content">
			<p>If there’s one specific day when I’m just working at home on my laptop, then I wash my hair in the morning and leave it to dry naturally for all day long. Oftentimes it will be taking the most of the day to dry which is not convenient at all. Though when using less frizz…</p>
			</div> 
			</div> 
			<footer class="entry-footer">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/17/see-these-incredible-photos-of-the-summer-trip-hairstyles/" class="btn btn-primary"><span class="btn__text">Read more</span><i class="glyphicon glyphicon-arrow-right"></i></a> </footer> 
			</article> 
			<article id="post-198" class="posts-list__item card post-thumbnail--fullwidth post-198 post type-post status-publish format-standard has-post-thumbnail hentry category-events category-haircuts tag-haircuts has-thumb">
			<div class="post-list__item-content">
			<figure class="post-thumbnail">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/16/golden-highlights/" class="post-thumbnail__link"><img class="post-thumbnail__img wp-post-image" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/04/image18-1280x510.jpg" alt="Golden Highlights" width="1280" height="510"></a>
			<div class="post__cats"><a href="https://ld-wp.template-help.com/wordpress_58991/category/events/" rel="tag">Events</a> <a href="https://ld-wp.template-help.com/wordpress_58991/category/haircuts/" rel="tag">Haircuts</a></div>
			</figure> 
			<header class="entry-header">
			<h4 class="entry-title"><a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/16/golden-highlights/" rel="bookmark">Golden Highlights</a></h4> </header> 
			<div class="entry-meta">
			<span class="post__date">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/16/" class="post__date-link"><time datetime="2016-04-16T11:26:48+00:00" title="2016-04-16T11:26:48+00:00">April 16</time></a> </span>
			<span class="posted-by">by <a href="https://ld-wp.template-help.com/wordpress_58991/author/admin/" class="posted-by__author" rel="author">admin</a></span>
			<span class="post__comments">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/16/golden-highlights/#respond" class="post__comments-link">comments (0)</a> </span>
			</div> 
			<div class="entry-content">
			<p>For quite a long time this has been one of our most frequently requested tutorials and it’s finally here! Though I personally wash my curly hair maybe once or twice a week, depending on the exact situation at hand. Like whether I’m shooting for the blog or where I’m going. It all depends on the…</p>
			</div> 
			</div> 
			<footer class="entry-footer">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/16/golden-highlights/" class="btn btn-primary"><span class="btn__text">Read more</span><i class="glyphicon glyphicon-arrow-right"></i></a> </footer> 
			</article> 
			<article id="post-221" class="posts-list__item card post-thumbnail--fullwidth post-221 post type-post status-publish format-image hentry category-haircuts category-tips tag-colors-in-trend tag-tips post_format-post-format-image no-thumb">
			<div class="post-list__item-content">
			<figure class="post-featured-content">
			<figure class="post-thumbnail"><a href="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/07/image67.jpg" class="post-thumbnail__link post-thumbnail--fullwidth" data-cherrypopup="1" data-init="{&quot;type&quot;:&quot;image&quot;}" data-popup="magnificPopup"><img width="1133" height="510" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/07/image67-1133x510.jpg" class="post-thumbnail__img" alt="image67"></a></figure>
			<div class="post__cats"><a href="https://ld-wp.template-help.com/wordpress_58991/category/haircuts/" rel="tag">Haircuts</a> <a href="https://ld-wp.template-help.com/wordpress_58991/category/tips/" rel="tag">Tips</a></div>
			</figure> 
			<header class="entry-header">
			<h4 class="entry-title"><a href="https://ld-wp.template-help.com/wordpress_58991/2016/03/30/image-format/" rel="bookmark">Image Format</a></h4> </header> 
			<div class="entry-meta">
			<span class="post__date">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/03/30/" class="post__date-link"><time datetime="2016-03-30T12:35:24+00:00" title="2016-03-30T12:35:24+00:00">March 30</time></a> </span>
			<span class="posted-by">by <a href="https://ld-wp.template-help.com/wordpress_58991/author/admin/" class="posted-by__author" rel="author">admin</a></span>
			<span class="post__comments">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/03/30/image-format/#respond" class="post__comments-link">comments (0)</a> </span>
			</div> 
			<div class="entry-content">
			<p>While the variety of colors and its hues that the US women ask their stylists about is huge, there are a few all-time favorites. The list of these colors starts with Ombre/Highlights, a pattern in which the color of the hair gradually changes from root to tip, either fading from dark to light or the…</p>
			</div> 
			</div> 
			<footer class="entry-footer">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/03/30/image-format/" class="btn btn-primary"><span class="btn__text">Read more</span><i class="glyphicon glyphicon-arrow-right"></i></a> </footer> 
			</article> 
			
			<article id="post-219" class="posts-list__item card post-thumbnail--fullwidth post-219 post type-post status-publish format-video hentry category-celebrities category-events tag-haircuts tag-tips post_format-post-format-video no-thumb">
			<div class="post-list__item-content">
			<div class="post-featured-content invert">
			<div class="entry-video embed-responsive embed-responsive-16by9"><iframe width="770" height="480" src="https://www.youtube.com/embed/LMl6Wvju-fM?feature=oembed" frameborder="0" allowfullscreen=""></iframe></div>
			<div class="post__cats"><a href="https://ld-wp.template-help.com/wordpress_58991/category/celebrities/" rel="tag">Celebrities</a> <a href="https://ld-wp.template-help.com/wordpress_58991/category/events/" rel="tag">Events</a></div>
			</div> 
			<header class="entry-header">
			<h4 class="entry-title"><a href="https://ld-wp.template-help.com/wordpress_58991/2016/03/25/video-format/" rel="bookmark">Video Format</a></h4> </header> 
			<div class="entry-meta">
			<span class="post__date">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/03/25/" class="post__date-link"><time datetime="2016-03-25T12:05:06+00:00" title="2016-03-25T12:05:06+00:00">March 25</time></a> </span>
			<span class="posted-by">by <a href="https://ld-wp.template-help.com/wordpress_58991/author/admin/" class="posted-by__author" rel="author">admin</a></span>
			<span class="post__comments">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/03/25/video-format/#respond" class="post__comments-link">comments (0)</a> </span>
			</div> 
			<div class="entry-content">
			<p>Today’s video tutorial will be about getting Smokey Green hair with some funky fresh Neon Green ends! I am sure, that anyone would be blown out when you see how our salon’s two best stylists create this color! Rickey was kind enough to open up his salon to us, so that’s why the scenery is…</p>
			</div> 
			</div> 
			<footer class="entry-footer">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/03/25/video-format/" class="btn btn-primary"><span class="btn__text">Read more</span><i class="glyphicon glyphicon-arrow-right"></i></a> </footer> 
			</article> 
			</div> 
			<nav class="navigation pagination" role="navigation">
			<h2 class="screen-reader-text">Posts navigation</h2>
			<!--<a class="prev page-numbers" href="https://ld-wp.template-help.com/wordpress_58991/news/"><i class="material-icons">navigate_before</i></a>-->
			<div class="nav-links">
			<span class="page-numbers current">1</span>
			<a class="page-numbers" href="https://ld-wp.template-help.com/wordpress_58991/news/page/2/">2</a>
			<a class="next page-numbers" href="https://ld-wp.template-help.com/wordpress_58991/news/page/2/"><i class="glyphicon glyphicon-chevron-right"></i></a></div>
			</nav>
			</main> 
			</div> 
			<div id="sidebar-primary" class="col-md-12 col-lg-3 sidebar-primary widget-area" role="complementary"><aside id="search-2" class="widget widget_search"><h4 class="widget-title">Search</h4><form role="search" method="get" class="search-form" action="https://ld-wp.template-help.com/wordpress_58991/">
			<label>
			<span class="screen-reader-text">Search for:</span>
			<input type="search" class="search-form__field" placeholder="Search …" value="" name="s" title="Search for:">
			</label>
			<button type="submit" class="search-form__submit btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>
			</form></aside><aside id="durand_widget_subscribe_follow-3" class="widget widget-subscribe"><div class="subscribe-block">
			<h4 class="widget-title">Get Our special offers</h4> <h4 class="subscribe-block__message">and the ultimate haircare tips &amp; tricks!</h4>
			<form method="POST" action="#" class="subscribe-block__form"><input type="hidden" id="durand_subscribe" name="durand_subscribe" value="f4e2a07765"><input type="hidden" name="_wp_http_referer" value="/wordpress_58991/news/"><div class="subscribe-block__input-group"><input class="subscribe-block__input" type="email" name="subscribe-mail" value="" placeholder="Your e-mail address"><a href="#" class="subscribe-block__submit btn">Subscribe</a></div><div class="subscribe-block__messages">
			<div class="subscribe-block__success hidden">You successfully subscribed</div>
			<div class="subscribe-block__error hidden"></div>
			</div></form>
			</div><div class="follow-block"><h4 class="widget-title">Let's Stay Connected</h4><div class="social-list social-list--widget social-list--icon"><ul id="social-list-1" class="social-list__items inline-list"><li id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12"><a href="https://www.facebook.com/TemplateMonster/"><span class="screen-reader-text">Facebook</span></a></li>
			<li id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13"><a href="https://twitter.com/TemplateMonster"><span class="screen-reader-text">Twitter</span></a></li>
			<li id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14"><a href="https://plus.google.com/+TemplateMonster"><span class="screen-reader-text">Google plus</span></a></li>
			<li id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16"><a href="https://www.linkedin.com/company/templatemonster-com"><span class="screen-reader-text">Linkedin</span></a></li>
			<li id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15"><a href="https://www.pinterest.com/templatemonster/"><span class="screen-reader-text">Pinterest</span></a></li>
			</ul></div></div><style scoped="">#durand_widget_subscribe_follow-3 .subscribe-block{background-color:#eaeaea}#durand_widget_subscribe_follow-3 .follow-block{background-color:#dedede}</style></aside><aside id="widget-custom-posts-2" class="widget widget-custom-posts custom-posts"><h4 class="widget-title">Recent Posts</h4><div class="custom-posts__holder row"><div class="custom-posts__item post col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div class="post-inner">
			<div class="entry-header">
			<div class="post-thumbnail">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/21/winterish-version-of-rainbow-hair/" class="post-thumbnail__link"><img class="post-thumbnail__img" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/04/image17-150x150.jpg" alt="Winterish version of rainbow hair?" width="150" height="150"></a> </div>
			<div class="post__cats"><a href="https://ld-wp.template-help.com/wordpress_58991/category/events/" rel="tag">Events</a></div> <h6><a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/21/winterish-version-of-rainbow-hair/" title="Winterish version of rainbow hair?">Winterish version of rainbow hair?</a></h6> <div class="entry-meta">
			<span class="posted-by">by <a href="https://ld-wp.template-help.com/wordpress_58991/author/admin/" class="posted-by__author" rel="author">admin</a></span> <span class="post__date"><a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/21/" class="post__date-link"><time datetime="2016-04-21T11:28:45+00:00">April 21</time></a></span> </div>
			</div>
			<div class="entry-content">
			</div>
			<div class="entry-footer">
			</div>
			</div>
			</div><div class="custom-posts__item post col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div class="post-inner">
			<div class="entry-header">
			<div class="post-thumbnail">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/17/see-these-incredible-photos-of-the-summer-trip-hairstyles/" class="post-thumbnail__link"><img class="post-thumbnail__img" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/04/image19-150x150.jpg" alt="See these incredible photos of the summer trip hairstyles" width="150" height="150"></a> </div>
			<div class="post__cats"><a href="https://ld-wp.template-help.com/wordpress_58991/category/colors-in-trend/" rel="tag">Colors in Trend</a></div> <h6><a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/17/see-these-incredible-photos-of-the-summer-trip-hairstyles/" title="See these incredible photos of the summer trip hairstyles">See these incredible photos of the summer trip hairstyles</a></h6> <div class="entry-meta">
			<span class="posted-by">by <a href="https://ld-wp.template-help.com/wordpress_58991/author/admin/" class="posted-by__author" rel="author">admin</a></span> <span class="post__date"><a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/17/" class="post__date-link"><time datetime="2016-04-17T11:27:54+00:00">April 17</time></a></span> </div>
			</div>
			<div class="entry-content">
			</div>
			<div class="entry-footer">
			</div>
			</div>
			</div><div class="custom-posts__item post col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div class="post-inner">
			<div class="entry-header">
			<div class="post-thumbnail">
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/16/golden-highlights/" class="post-thumbnail__link"><img class="post-thumbnail__img" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/04/image18-150x150.jpg" alt="Golden Highlights" width="150" height="150"></a> </div>
			<div class="post__cats"><a href="https://ld-wp.template-help.com/wordpress_58991/category/events/" rel="tag">Events</a> <a href="https://ld-wp.template-help.com/wordpress_58991/category/haircuts/" rel="tag">Haircuts</a></div> <h6><a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/16/golden-highlights/" title="Golden Highlights">Golden Highlights</a></h6> <div class="entry-meta">
			<span class="posted-by">by <a href="https://ld-wp.template-help.com/wordpress_58991/author/admin/" class="posted-by__author" rel="author">admin</a></span> <span class="post__date"><a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/16/" class="post__date-link"><time datetime="2016-04-16T11:26:48+00:00">April 16</time></a></span> </div>
			</div>
			<div class="entry-content">
			</div>
			<div class="entry-footer">
			</div>
			</div>
			</div></div></aside><aside id="durand_widget_about_author-2" class="widget durand widget-about-author"><h4 class="widget-title">About Durand</h4><div class="about-author"><div class="about-author_avatar"><img class="about-author_img" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/06/image46-512x442.jpg" width="250" height="250" alt="avatar"></div><div class="about-author_content"><h4 class="about-author_name">admin</h4><div class="about-author_description">Throughout high school and college, she was famous for her talent of cutting hair and creating unique hairstyles. Shane Doe decided to convert her hobby into a profession, she was most excited to make people feel good about themselves.</div><div class="about-author_btn_box"><a href="#" class="about-author_btn btn">Read More</a></div></div>
			</div>
			</aside><aside id="categories-3" class="widget widget_categories"><h4 class="widget-title">Categories</h4><label class="screen-reader-text" for="cat">Categories</label><select name="cat" id="cat" class="postform">
			<option value="-1">Select Category</option>
			<option class="level-0" value="53">Celebrities</option>
			<option class="level-0" value="52">Colors in Trend</option>
			<option class="level-0" value="55">Events</option>
			<option class="level-0" value="51">Haircuts</option>
			<option class="level-0" value="54">Tips</option>
			<option class="level-0" value="1">Uncategorized</option>
			</select>
			
			</aside><aside id="archives-3" class="widget widget_archive"><h4 class="widget-title">Archives</h4> <label class="screen-reader-text" for="archives-dropdown-3">Archives</label>
			<select id="archives-dropdown-3" name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
			<option value="">Select Month</option>
			<option value="https://ld-wp.template-help.com/wordpress_58991/2016/05/"> May 2016 </option>
			<option value="https://ld-wp.template-help.com/wordpress_58991/2016/04/"> April 2016 </option>
			<option value="https://ld-wp.template-help.com/wordpress_58991/2016/03/"> March 2016 </option>
			<option value="https://ld-wp.template-help.com/wordpress_58991/2015/02/"> February 2015 </option>
			</select>
			</aside> <aside id="recent-posts-2" class="widget widget_recent_entries"> <h4 class="widget-title">Recent Posts</h4> <ul>
			<li>
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/05/03/how-to-wash-and-style-curly-hair/">How to wash and style curly hair</a>
			</li>
			<li>
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/24/returning-to-a-natural-hair-texture/">Returning to a natural hair texture</a>
			</li>
			<li>
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/21/winterish-version-of-rainbow-hair/">Winterish version of rainbow hair?</a>
			</li>
			</ul>
			</aside> </div>
			</div> 
			</div> 
			</div>
		<footer id="colophon" class="site-footer default container" role="contentinfo">
			<div class="footer-full-width-area-wrap">
			<div class="container">
			<section id="footer-full-width-area" class="footer-full-width-area widget-area"><aside id="durand_widget_subscribe_follow-2" class="widget widget-subscribe"><div class="subscribe-block">
			<h2 class="widget-title">Get Our special offers</h2> <h4 class="subscribe-block__message">and the ultimate hair care tips &amp; tricks!</h4>
			<form method="POST" action="#" class="subscribe-block__form"><input type="hidden" id="durand_subscribe" name="durand_subscribe" value="34f8ef18eb"><input type="hidden" name="_wp_http_referer" value="/wordpress_58991/"><div class="subscribe-block__input-group"><input class="subscribe-block__input" type="email" name="subscribe-mail" value="" placeholder="Your e-mail address"><a href="#" class="subscribe-block__submit btn">Subscribe</a></div><div class="subscribe-block__messages">
			<div class="subscribe-block__success hidden">You successfully subscribed</div>
			<div class="subscribe-block__error hidden"></div>
			</div></form>
			</div><div class="follow-block"><h2 class="widget-title">Let's Stay Connected</h2><div class="social-list social-list--widget social-list--icon"><ul id="social-list-1" class="social-list__items inline-list"><li id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12"><a href="https://www.facebook.com/TemplateMonster/"><span class="screen-reader-text">Facebook</span></a></li>
			<li id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13"><a href="https://twitter.com/TemplateMonster"><span class="screen-reader-text">Twitter</span></a></li>
			<li id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14"><a href="https://plus.google.com/+TemplateMonster"><span class="screen-reader-text">Google plus</span></a></li>
			<li id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16"><a href="https://www.linkedin.com/company/templatemonster-com"><span class="screen-reader-text">Linkedin</span></a></li>
			<li id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15"><a href="https://www.pinterest.com/templatemonster/"><span class="screen-reader-text">Pinterest</span></a></li>
			</ul></div></div></aside></section> </div>
			</div>
			<div class="footer-area-wrap invert">
			<div class="container">
			<section id="footer-area" class="footer-area widget-area row"><aside id="durand_widget_about-2" class="col-xs-12 col-sm-6 col-md-3  widget widget-about">
			<div class="widget-about__logo">
			<a class="widget-about__logo-link" href="https://ld-wp.template-help.com/wordpress_58991/">
			<img class="widget-about__logo-img" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/07/logo_about.png" alt="Durand">
			</a>
			</div>
			<div class="widget-about__tagline"></div>
			<div class="widget-about__content"><div class="tm_builder_outer_content" id="tm_builder_outer_content">
			<div class="tm_builder_inner_content tm_pb_gutters3">
			<p>Yearly we help thousands of women in New York to highlight their beauty, care about their precious hair and all in all make them feel special!</p>
			</div>
			</div></div></aside><aside id="nav_menu-4" class="col-xs-12 col-sm-6 col-md-3  widget widget_nav_menu"><h5 class="widget-title">navigation</h5><div class="menu-custom-menu-container"><ul id="menu-custom-menu" class="menu"><li id="menu-item-1162" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1162"><a href="https://ld-wp.template-help.com/wordpress_58991/about-us/">About us</a></li>
			<li id="menu-item-1066" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1066"><a href="https://ld-wp.template-help.com/wordpress_58991/services/">Services</a></li>
			<li id="menu-item-1064" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1064"><a href="https://ld-wp.template-help.com/wordpress_58991/our-staff/">Our staff</a></li>
			<li id="menu-item-1063" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1063"><a href="https://ld-wp.template-help.com/wordpress_58991/schedule/">Schedule</a></li>
			<li id="menu-item-1065" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1065"><a href="https://ld-wp.template-help.com/wordpress_58991/news/">News</a></li>
			<li id="menu-item-1618" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1618"><a href="https://ld-wp.template-help.com/wordpress_58991/contacts/">Contacts</a></li>
			</ul></div></aside><aside id="text-4" class="col-xs-12 col-sm-6 col-md-3  widget widget_text"><h5 class="widget-title">Contact Information</h5> <div class="textwidget"><p>4096 N Highland St, Arlington, VA</p>
			<p>32101, USA</p>
			<a href="mailto:ironmass@demolink.org">durand@demolink.org</a></div>
			</aside><aside id="text-5" class="col-xs-12 col-sm-6 col-md-3  widget widget_text"><h5 class="widget-title">We are open</h5> <div class="textwidget"><p>Mon-Thu: 9:30 - 21:00</p>
			<p>Fri: 6:00 - 21:00</p>
			<p>Sat: 10:00 - 15:00</p></div>
			</aside></section> </div>
			</div>
			<div class="footer-container">
			<div class="site-info container">
			<div class="site-info__mid-box">
			<div class="footer-copyright">© 2017 All rights reserved by DURAND</div> <nav id="footer-navigation" class="footer-menu" role="navigation">
			
			</ul> </nav> 
			</div>
			</div> 
			</div> 
		</footer>
	</div>
		
	<!-- Bootstrap Core JavaScript -->
	<script src="public/js/jquery.js"></script>

    
    <script src="public/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="resources/assets/nivoslider/jquery.nivo.slider.js"></script>
	<script>
		$('.carousel').carousel({
			interval: 5000 //changes the speed
		})
    </script>
	<script type="text/javascript">
		$(window).load(function() {
			$('#slider').nivoSlider();
		});
		var flag = 0;
		var flag1 = 0;
		$(".menu-toggle").click(function(){

			if(flag == 0){
				$(".main-navigation").addClass("toggled");
				flag = 1 ;
			}else{
				$(".main-navigation").removeClass("toggled");
				flag = 0 ;
			}		
		});
		$(".sub-menu-toggle").click(function(){
			if(flag == 0){
				$(this).addClass("active");
				$(this).parent().addClass("sub-menu-open");
				flag = 1 ;
			}else{
				$(this).parent().removeClass("sub-menu-open");
				$(this).removeClass("active");
				flag = 0 ;
			}			
		});
		$(".tm-pb-arrow-next").off().on("click",function(){
			 $(".tm_pb_slide").each(function(){
				var index = $(".tm_pb_slide").index(this);
				if(index == 0){
					$(this).addClass("tm-pb-moved-slide");
				}
				if(index ==1){
					$(this).addClass("tm-pb-active-slide");	
				}
				if(index ==2){
					$(this).css("z-index",1)
				}
				if($(this).is(':visible')){
					var index = $('.tm_pb_slide').index(this);
					console.log(index);
				}
				console.log(index);
			 });
			 
			
		});
	</script>
</body>
</html>