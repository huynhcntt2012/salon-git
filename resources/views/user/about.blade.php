<!DOCTYPE html>
<html lang="en">
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link href="public/css/bootstrap.min.css" rel="stylesheet">
	<link href="public/css/index.css" rel="stylesheet">
    <link href="public/css/about.css" rel="stylesheet">
	<link href="public/css/footer.css" rel="stylesheet">
	<link href="public/css/full-slider.css" rel="stylesheet">
	
	<link rel="stylesheet" href="resources/assets/nivoslider/themes/custom/custom.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resources/assets/nivoslider/nivo-slider.css" type="text/css" media="screen" />
	<link href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
</head>
<body>
	<div id="page" class="site">
		<header id="masthead" class="site-header centered container" role="banner">
			<div class="top-panel"> 
				<div class="top-panel__wrap container">
					<div class="top-panel__message">
						<div class="info-block">
							<i class="glyphicon glyphicon-earphone"></i> Call us today: <a href="tel:1-555-644-5566">1-555-644-5566</a>
						</div>
						<div class="info-block">
							<i class="glyphicon glyphicon-map-marker"></i> 7087 Richmond hwy, Alexandria, VA
						</div>
					</div>
					<div class="top-panel__search">
						<form role="search" method="get" class="search-form" action="">
							<label>
							<span class="screen-reader-text">Search for:</span>
							<input type="search" class="search-form__field" placeholder="Search …" value="" name="s" title="Search for:">
							</label>
							<button type="submit" class="search-form__submit btn btn-primary"><i class="material-icons glyphicon glyphicon-search"></i></button>
						</form>
					</div>
				</div>
			</div>
			<div class="header-container">
				<div class="header-container_wrap container">
					<div class="header-container__flex">
						<div class="header-container__center">
							<div class="site-branding">
								<div class="site-logo">
									<a class="site-logo__link" href="https://ld-wp.template-help.com/wordpress_58991/" rel="home">Durand</a>
								</div>
							</div>
							<nav id="site-navigation" class="main-navigation" role="navigation">
								<button class="menu-toggle" aria-controls="main-menu" aria-expanded="false">
								<i class="menu-toggle__icon fa fa-bars" aria-hidden="true"></i>
								Menu </button>
								<ul id="main-menu" class="menu">
                                    <li id="menu-item-1163" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1163"><a href="home">Home</a>
								
								<span class="sub-menu-toggle"></span></li>
								<li id="menu-item-414" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-414">
                                <a href="about">about us</a>
								
								<span class="sub-menu-toggle"></span></li>
								<li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="gallery">Gallery</a>
                                    <ul class="sub-menu">
        								<li id="menu-item-755" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-755"><a href="gallery/image">Image</a></li>
        								<li id="menu-item-756" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-756"><a href="gallery/video">Video</a></li>
								    </ul>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="services">Services</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="blog">blog</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="contact">Contact</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="login">Login</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
								</ul> 
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div id="content" class="site-content">
        
        
			<div class="breadcrumbs"><div class="container"><div class="breadcrumbs__title"></div><div class="breadcrumbs__items">
				<div class="breadcrumbs__content">
				<div class="breadcrumbs__wrap"><div class="breadcrumbs__item"><a href="https://ld-wp.template-help.com/wordpress_58991/" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
				<div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div> <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">About us</span></div>
				</div>
				</div></div><div class="clear"></div></div>
				</div>
				<div class="site-content_wrap">
				<div class="row">
				<div id="primary" class="col-xs-12 col-md-12">
				<main id="main" class="site-main" role="main">
				<article id="post-1118" class="post-1118 page type-page status-publish hentry no-thumb">
				<header class="entry-header">
				<h1 class="entry-title screen-reader-text">About us</h1> </header> 
				<div class="entry-content">
				<div class="tm_builder_outer_content" id="tm_builder_outer_content">
				<div class="tm_builder_inner_content tm_pb_gutters3">
				<div class="tm_pb_section  tm_pb_section_0 tm_section_regular tm_section_transparent">
				<div class="container">
				<div class=" row tm_pb_row tm_pb_row_0">
				<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_0 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_center main-title tm_pb_text_0">
				<h2 style="text-align: center;">Welcome to the <em>best</em> hairdressing salon <em>in New York!</em></h2>
				</div>  
				</div>  
				</div>  
				</div><div class="container">
				<div class=" row tm_pb_row tm_pb_row_1">
				<div class="tm_pb_column tm_pb_column_1_2  tm_pb_column_1 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
				<div class="tm_pb_module tm-waypoint tm_pb_image tm_pb_animation_off tm_pb_image_0 tm_always_center_on_mobile tm-animated">
				<img src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/06/image58.jpg" alt="">
				</div><hr class="tm_pb_module tm_pb_space tm_pb_divider tm_pb_divider_0"><div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left  tm_pb_text_1">
				<h4>The History</h4>
				<p>This salon, founded by three best friends Mary, Brenda and Alberta is now the New York’s women staple for either getting a nice and beautiful hairdo, chit chatting with their favorite stylist and just to feel special again…</p>
				<h4>The Teamwork</h4>
				<p>Everything that we do at the salon we do as a team… Despite all of our stylists being of different age, social background, and heritage, we’re all looking for just one simple goal in sight – namely, to fulfill the ever-evolving needs of New York’s charming ladies!</p>
				</div>  
				</div>  <div class="tm_pb_column tm_pb_column_1_2  tm_pb_column_2 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
				<div class="tm_pb_module tm-waypoint tm_pb_image tm_pb_animation_off tm_pb_image_1 tm_always_center_on_mobile tm-animated">
				<img src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/06/image59.jpg" alt="">
				</div><hr class="tm_pb_module tm_pb_space tm_pb_divider tm_pb_divider_1"><div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left  tm_pb_text_2">
				<h4>The Wide Range of Services</h4>
				<p>Starting with offering traditional service of female, male and children’s haircuts, and all the way to Hair coloringing, hair care treatments, and special occasion hairdos, we’re eager to offer you more hairdressing services than anybody else in the city!</p>
				<h4>The Atmosphere</h4>
				<p>It’s no wonder, that our returning customers total to 85% of all of our clients, and the new people get used to our lovely atmosphere at once. Besides teas, coffees and even salads (which are free of charge) our charming stylists also always offer a nice chit-chat while they do you a hairdo!</p>
				</div>  
				</div>  
				</div>  
				</div>
				</div>  <div class="tm_pb_section  tm_pb_section_1 tm_section_regular tm_section_transparent">
				<div class="container">
				<div class=" row tm_pb_row tm_pb_row_2">
				<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_3 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<hr class="tm_pb_module tm_pb_space tm_pb_divider tm_pb_divider_2">
				</div>  
				</div>  
				</div><div class="container">
				<div class="row tm_pb_row tm_pb_row_3 tm_pb_row_4col">
				<div class="tm_pb_column tm_pb_column_1_4  tm_pb_column_4 col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
				<div class="tm_pb_circle_counter tm_pb_circle_counter_0 tm_pb_bg_layout_light tm_pb_module"><div class="tm_pb_circle_counter_bar container-width-change-notify" data-number-value="12" data-bar-bg-color="#ffffff" data-size="230" data-bar-type="round" data-color="#d28881" data-alpha="1" data-circle-width="4">
				<div class="percent">
				<p style="visibility: visible;">
				<span class="percent-value">12</span> </p>
				</div>
				<h3>Services</h3><canvas height="230" width="230"></canvas></div></div> 
				</div>  <div class="tm_pb_column tm_pb_column_1_4  tm_pb_column_5 col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
				<div class="tm_pb_circle_counter tm_pb_circle_counter_1 tm_pb_bg_layout_light tm_pb_module"><div class="tm_pb_circle_counter_bar container-width-change-notify" data-number-value="20" data-bar-bg-color="#ffffff" data-size="230" data-bar-type="round" data-color="#d28881" data-alpha="1" data-circle-width="4">
				<div class="percent">
				<p style="visibility: visible;">
				<span class="percent-value">20</span> </p>
				</div>
				<h3>Awards</h3><canvas height="230" width="230"></canvas></div></div> 
				</div>  <div class="tm_pb_column tm_pb_column_1_4  tm_pb_column_6 col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
				<div class="tm_pb_circle_counter tm_pb_circle_counter_2 tm_pb_bg_layout_light tm_pb_module"><div class="tm_pb_circle_counter_bar container-width-change-notify" data-number-value="10" data-bar-bg-color="#ffffff" data-size="230" data-bar-type="round" data-color="#d28881" data-alpha="1" data-circle-width="4">
				<div class="percent">
				<p style="visibility: visible;">
				<span class="percent-value">10</span> </p>
				</div>
				<h3>Stylists</h3><canvas height="230" width="230"></canvas></div></div> 
				</div>  <div class="tm_pb_column tm_pb_column_1_4  tm_pb_column_7 col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
				<div class="tm_pb_circle_counter tm_pb_circle_counter_3 tm_pb_bg_layout_light tm_pb_module"><div class="tm_pb_circle_counter_bar container-width-change-notify" data-number-value="92" data-bar-bg-color="#ffffff" data-size="230" data-bar-type="round" data-color="#d28881" data-alpha="1" data-circle-width="4">
				<div class="percent">
				<p style="visibility: visible;">
				<span class="percent-value">92</span> </p>
				</div>
				<h3>Discount club members</h3><canvas height="230" width="230"></canvas></div></div> 
				</div>  
				</div>  
				</div><div class="container">
				<div class=" row tm_pb_row tm_pb_row_4">
				<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_8 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<hr class="tm_pb_module tm_pb_space tm_pb_divider tm_pb_divider_3">
				</div>  
				</div>  
				</div>
				</div>  <div class="tm_pb_section  tm_pb_section_2 tm_section_regular tm_section_transparent">
				<div class="container">
				<div class=" row tm_pb_row tm_pb_row_5">
				<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_9 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div id="myCarousel" class="carousel slide">
													<!-- Indicators -->

													<!-- Wrapper for Slides -->
													<div class="carousel-inner">
														<div class="item">
															<!-- Set the first background image using inline CSS below. -->
															<div class="fill" style="background-image:url('https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image8.jpg');"></div>
															
																	
															
														</div>
														<div class="item">
															<!-- Set the second background image using inline CSS below. -->
															<div class="fill" style="background-image:url('https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image10.jpg');"></div>
															
														</div>
														<div class="item active">
															<!-- Set the third background image using inline CSS below. -->
															<div class="fill" style="background-image:url('https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image9.jpg');"></div>
															
														</div>
													</div>

													<!-- Controls -->
													<a class="left carousel-control custom" href="#myCarousel" data-slide="prev">
														<span class="icon-prev"></span>
													</a>
													<a class="right carousel-control custom" href="#myCarousel" data-slide="next">
														<span class="icon-next"></span>
													</a>

												</div>
												</div>
				</div>  
				</div>  
				</div>
				</div>  <div class="tm_pb_section  tm_pb_section_3 tm_pb_with_background tm_section_regular">
				<div class="container">
				<div class=" row tm_pb_row tm_pb_row_6">
				<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_10 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_center main-title tm_pb_text_3">
				<h2 style="text-align: center;">testimonial</h2>
				</div>  <div class="tm_pb_testimonial tm_pb_testimonial_0 tm_pb_bg_layout_light tm_pb_text_align_center  clearfix tm_pb_module" style="background-color: rgba(245,245,245,0);" data-icon=""><div class="tm_pb_testimonial_portrait" style="background-image: url(https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image7.jpg);">
				</div><div class="tm_pb_testimonial_description">
				<div class="tm_pb_testimonial_description_inner" style="width: 1030px;">
				<div class="tm_pb_testimonial_quote_icon" data-icon=""></div>
				<p>I’ve been a customer of this same hairdressing salon since I moved in to New York. The main stylists didn’t change and they’re still just as good as few years back. God only knows, how many great things these ladies did to my hair over the years…</p>
				<strong class="tm_pb_testimonial_author">
				Geraldina Durrell </strong>
				<p class="tm_pb_testimonial_meta"></p>
				</div> 
				</div> 
				</div> 
				</div>  
				</div>  
				</div>
				</div>  
				</div>
				</div> </div> 
				<footer class="entry-footer">
				</footer> 
				</article> 
				</main> 
				</div> 
				</div>
            
            
            
		</div>
		<footer id="colophon" class="site-footer default container" role="contentinfo">
			<div class="footer-full-width-area-wrap">
			<div class="container">
			<section id="footer-full-width-area" class="footer-full-width-area widget-area"><aside id="durand_widget_subscribe_follow-2" class="widget widget-subscribe"><div class="subscribe-block">
			<h2 class="widget-title">Get Our special offers</h2> <h4 class="subscribe-block__message">and the ultimate hair care tips &amp; tricks!</h4>
			<form method="POST" action="#" class="subscribe-block__form"><input type="hidden" id="durand_subscribe" name="durand_subscribe" value="34f8ef18eb"><input type="hidden" name="_wp_http_referer" value="/wordpress_58991/"><div class="subscribe-block__input-group"><input class="subscribe-block__input" type="email" name="subscribe-mail" value="" placeholder="Your e-mail address"><a href="#" class="subscribe-block__submit btn">Subscribe</a></div><div class="subscribe-block__messages">
			<div class="subscribe-block__success hidden">You successfully subscribed</div>
			<div class="subscribe-block__error hidden"></div>
			</div></form>
			</div><div class="follow-block"><h2 class="widget-title">Let's Stay Connected</h2><div class="social-list social-list--widget social-list--icon"><ul id="social-list-1" class="social-list__items inline-list"><li id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12"><a href="https://www.facebook.com/TemplateMonster/"><span class="screen-reader-text">Facebook</span></a></li>
			<li id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13"><a href="https://twitter.com/TemplateMonster"><span class="screen-reader-text">Twitter</span></a></li>
			<li id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14"><a href="https://plus.google.com/+TemplateMonster"><span class="screen-reader-text">Google plus</span></a></li>
			<li id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16"><a href="https://www.linkedin.com/company/templatemonster-com"><span class="screen-reader-text">Linkedin</span></a></li>
			<li id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15"><a href="https://www.pinterest.com/templatemonster/"><span class="screen-reader-text">Pinterest</span></a></li>
			</ul></div></div></aside></section> </div>
			</div>
			<div class="footer-area-wrap invert">
			<div class="container">
			<section id="footer-area" class="footer-area widget-area row"><aside id="durand_widget_about-2" class="col-xs-12 col-sm-6 col-md-3  widget widget-about">
			<div class="widget-about__logo">
			<a class="widget-about__logo-link" href="https://ld-wp.template-help.com/wordpress_58991/">
			<img class="widget-about__logo-img" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/07/logo_about.png" alt="Durand">
			</a>
			</div>
			<div class="widget-about__tagline"></div>
			<div class="widget-about__content"><div class="tm_builder_outer_content" id="tm_builder_outer_content">
			<div class="tm_builder_inner_content tm_pb_gutters3">
			<p>Yearly we help thousands of women in New York to highlight their beauty, care about their precious hair and all in all make them feel special!</p>
			</div>
			</div></div></aside><aside id="nav_menu-4" class="col-xs-12 col-sm-6 col-md-3  widget widget_nav_menu"><h5 class="widget-title">navigation</h5><div class="menu-custom-menu-container"><ul id="menu-custom-menu" class="menu"><li id="menu-item-1162" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1162"><a href="https://ld-wp.template-help.com/wordpress_58991/about-us/">About us</a></li>
			<li id="menu-item-1066" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1066"><a href="https://ld-wp.template-help.com/wordpress_58991/services/">Services</a></li>
			<li id="menu-item-1064" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1064"><a href="https://ld-wp.template-help.com/wordpress_58991/our-staff/">Our staff</a></li>
			<li id="menu-item-1063" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1063"><a href="https://ld-wp.template-help.com/wordpress_58991/schedule/">Schedule</a></li>
			<li id="menu-item-1065" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1065"><a href="https://ld-wp.template-help.com/wordpress_58991/news/">News</a></li>
			<li id="menu-item-1618" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1618"><a href="https://ld-wp.template-help.com/wordpress_58991/contacts/">Contacts</a></li>
			</ul></div></aside><aside id="text-4" class="col-xs-12 col-sm-6 col-md-3  widget widget_text"><h5 class="widget-title">Contact Information</h5> <div class="textwidget"><p>4096 N Highland St, Arlington, VA</p>
			<p>32101, USA</p>
			<a href="mailto:ironmass@demolink.org">durand@demolink.org</a></div>
			</aside><aside id="text-5" class="col-xs-12 col-sm-6 col-md-3  widget widget_text"><h5 class="widget-title">We are open</h5> <div class="textwidget"><p>Mon-Thu: 9:30 - 21:00</p>
			<p>Fri: 6:00 - 21:00</p>
			<p>Sat: 10:00 - 15:00</p></div>
			</aside></section> </div>
			</div>
			<div class="footer-container">
			<div class="site-info container">
			<div class="site-info__mid-box">
			<div class="footer-copyright">© 2017 All rights reserved by DURAND</div> <nav id="footer-navigation" class="footer-menu" role="navigation">
			
			</ul> </nav> 
			</div>
			</div> 
			</div> 
		</footer>
	</div>
		
	<!-- Bootstrap Core JavaScript -->
	<script src="public/js/jquery.js"></script>

    
    <script src="public/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="resources/assets/nivoslider/jquery.nivo.slider.js"></script>
	<script>
		$('.carousel').carousel({
			interval: 5000 //changes the speed
		})
    </script>
	<script type="text/javascript">
		$(window).load(function() {
			$('#slider').nivoSlider();
		});
		var flag = 0;
		var flag1 = 0;
		$(".menu-toggle").click(function(){

			if(flag == 0){
				$(".main-navigation").addClass("toggled");
				flag = 1 ;
			}else{
				$(".main-navigation").removeClass("toggled");
				flag = 0 ;
			}		
		});
		$(".sub-menu-toggle").click(function(){
			if(flag == 0){
				$(this).addClass("active");
				$(this).parent().addClass("sub-menu-open");
				flag = 1 ;
			}else{
				$(this).parent().removeClass("sub-menu-open");
				$(this).removeClass("active");
				flag = 0 ;
			}			
		});
		$(".tm-pb-arrow-next").off().on("click",function(){
			 $(".tm_pb_slide").each(function(){
				var index = $(".tm_pb_slide").index(this);
				if(index == 0){
					$(this).addClass("tm-pb-moved-slide");
				}
				if(index ==1){
					$(this).addClass("tm-pb-active-slide");	
				}
				if(index ==2){
					$(this).css("z-index",1)
				}
				if($(this).is(':visible')){
					var index = $('.tm_pb_slide').index(this);
					console.log(index);
				}
				console.log(index);
			 });
			 
			
		});
	</script>
</body>
</html>