<!DOCTYPE html>
<html lang="en">
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link href="public/css/bootstrap.min.css" rel="stylesheet">
	<link href="public/css/index.css" rel="stylesheet">
	<link href="public/css/contact.css" rel="stylesheet">
	<link href="public/css/footer.css" rel="stylesheet">
	<link href="public/css/full-slider.css" rel="stylesheet">
	
	<link rel="stylesheet" href="resources/assets/nivoslider/themes/custom/custom.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resources/assets/nivoslider/nivo-slider.css" type="text/css" media="screen" />
	<link href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
</head>
<body>
	<div id="page" class="site">
		<header id="masthead" class="site-header centered container" role="banner">
			<div class="top-panel"> 
				<div class="top-panel__wrap container">
					<div class="top-panel__message">
						<div class="info-block">
							<i class="glyphicon glyphicon-earphone"></i> Call us today: <a href="tel:1-555-644-5566">1-555-644-5566</a>
						</div>
						<div class="info-block">
							<i class="glyphicon glyphicon-map-marker"></i> 7087 Richmond hwy, Alexandria, VA
						</div>
					</div>
					<div class="top-panel__search">
						<form role="search" method="get" class="search-form" action="">
							<label>
							<span class="screen-reader-text">Search for:</span>
							<input type="search" class="search-form__field" placeholder="Search …" value="" name="s" title="Search for:">
							</label>
							<button type="submit" class="search-form__submit btn btn-primary"><i class="material-icons glyphicon glyphicon-search"></i></button>
						</form>
					</div>
				</div>
			</div>
			<div class="header-container">
				<div class="header-container_wrap container">
					<div class="header-container__flex">
						<div class="header-container__center">
							<div class="site-branding">
								<div class="site-logo">
									<a class="site-logo__link" href="https://ld-wp.template-help.com/wordpress_58991/" rel="home">Durand</a>
								</div>
							</div>
							<nav id="site-navigation" class="main-navigation" role="navigation">
								<button class="menu-toggle" aria-controls="main-menu" aria-expanded="false">
								<i class="menu-toggle__icon fa fa-bars" aria-hidden="true"></i>
								Menu </button>
								<ul id="main-menu" class="menu">
                                    <li id="menu-item-1163" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1163"><a href="home">Home</a>
								
								<span class="sub-menu-toggle"></span></li>
								<li id="menu-item-414" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-414">
                                <a href="about">about us</a>
								
								<span class="sub-menu-toggle"></span></li>
								<li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="gallery">Gallery</a>
                                    <ul class="sub-menu">
        								<li id="menu-item-755" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-755"><a href="gallery/image">Image</a></li>
        								<li id="menu-item-756" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-756"><a href="gallery/video">Video</a></li>
								    </ul>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="services">Services</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="blog">blog</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="contact">Contact</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="login">Login</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
								</ul> 
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>
        
        
        
        
        <div id="content" class="site-content">
<div class="breadcrumbs"><div class="container"><div class="breadcrumbs__title"></div><div class="breadcrumbs__items">
<div class="breadcrumbs__content">
<div class="breadcrumbs__wrap"><div class="breadcrumbs__item"><a href="https://ld-wp.template-help.com/wordpress_58991/" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
<div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">&#47;</div></div> <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">Contacts</span></div>
</div>
</div></div><div class="clear"></div></div>
</div>
<div class="site-content_wrap">
<div class="row">
<div id="primary" class="col-xs-12 col-md-12">
<main id="main" class="site-main" role="main">
<article id="post-713" class="post-713 page type-page status-publish hentry no-thumb">
<header class="entry-header">
<h1 class="entry-title screen-reader-text">Contacts</h1> </header> 
<div class="entry-content">
<div class="tm_builder_outer_content" id="tm_builder_outer_content">
<div class="tm_builder_inner_content tm_pb_gutters3">
<div class="tm_pb_section  tm_pb_section_0 tm_section_regular tm_section_transparent">
<div class="container">
<div class=" row tm_pb_row tm_pb_row_0">
<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_0 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
<div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left main-title tm_pb_text_0">
<h2 style="text-align: center;">Our <em>Location</em></h2>
</div>  <div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left  tm_pb_text_1">
<h4 style="text-align: center;">Visit the best hairdressing salon in New York!</h4>
<p style="text-align: center;">425 W. 14th Street, 2nd FloorNew York, New York 10014</p>
</div>  <hr class="tm_pb_module tm_pb_space tm_pb_divider_0"/><div class="tm_pb_module tm_pb_map_container  tm_pb_map_0">
<div class="tm_pb_map" data-center-lat="40.71088680518549" data-center-lng="-74.00152101954347" data-zoom="15" data-mouse-wheel="off" data-marker-icon='["http:\/\/ld-wp.template-help.com\/wordpress_58991\/wp-content\/uploads\/2016\/06\/marker.png",34,53,false]' data-map-style='[{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]'></div>
<div class="tm_pb_map_pin" data-lat="40.7127837" data-lng="-74.00594130000002" data-title="">
<h3 style="margin-top: 10px;"></h3>
<div class="infowindow">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>11559<br/>
Ventura Boulevard<br/>
Studio City,<br/>
CA 91604</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>
</div>
</div>
</div>  
</div>  
</div>
</div>  <div class="tm_pb_section  tm_pb_section_1 tm_pb_with_background tm_section_regular">
<div class="container">
<div class=" row tm_pb_row tm_pb_row_1">
<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_1 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
<div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left main-title tm_pb_text_2">
<h2 style="text-align: center;">Contact Us <em>Today!</em></h2>
</div>  <div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left  tm_pb_text_3">
<h4 style="text-align: center;">Get in touch with us regarding any questions on our services<br/>
or simply to book an appointment!</h4>
</div>  
</div>  
</div>  
</div><div class="container">
<div class=" row tm_pb_row tm_pb_row_2">
<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_2 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
<div id="tm_pb_contact_form_0" class="tm_pb_contact_form tm_pb_contact_form_0 tm_pb_contact_form_container clearfix tm_pb_module" data-form_unique_num='0'><h2 class="tm_pb_contact_main_title"></h2>
<form class="tm_pb_contact_form clearfix" method="post" action="https://ld-wp.template-help.com/wordpress_58991/contacts/">
<div class="tm-pb-contact-message"></div>
<div class="row">
<div class="tm_pb_contact_field col-md-12 ">
<label for="tm_pb_contact_name_1" class="tm_pb_contact_form_label">Your Name:</label>
<input type="text" id="tm_pb_contact_name_1" class="tm_pb_contact_form_input" value="" name="tm_pb_contact_name_1" data-required_mark="required" data-field_type="input" data-original_id="name" data-original_title="Your Name:"></div><div class="tm_pb_contact_field col-md-12 ">
<label for="tm_pb_contact_email_1" class="tm_pb_contact_form_label">Your E-mail:</label>
<input type="email" id="tm_pb_contact_email_1" class="tm_pb_contact_form_input" value="" name="tm_pb_contact_email_1" data-required_mark="required" data-field_type="email" data-original_id="email" data-original_title="Your E-mail:"></div><div class="tm_pb_contact_field col-md-12 ">
<label for="tm_pb_contact_message_1" class="tm_pb_contact_form_label">Your Review:</label>
<textarea name="tm_pb_contact_message_1" id="tm_pb_contact_message_1" class="tm_pb_contact_message tm_pb_contact_form_input" data-required_mark="required" data-field_type="text" data-original_id="message" data-original_title="Your Review:"></textarea></div>
</div>
<input type="hidden" value="tm_contact_proccess" name="tm_pb_contactform_submit_0">
<input type="text" value="" name="tm_pb_contactform_validate_0" class="tm_pb_contactform_validate_field"/>
<div class="tm_contact_bottom_container">
<div class="tm_pb_contact_right">
<div class="clearfix">
<span class="tm_pb_contact_captcha_question">2 + 9</span> = <input type="text" size="2" class="tm_pb_contact_captcha" data-original_title="Captcha" data-first_digit="2" data-second_digit="9" value="" name="tm_pb_contact_captcha_0" data-required_mark="required">
</div>
</div>  
<button type="submit" class="tm_pb_contact_submit tm_pb_button">Submit</button> </div>
<input type="hidden" id="_wpnonce-tm-pb-contact-form-submitted" name="_wpnonce-tm-pb-contact-form-submitted" value="1ed8771b29"/><input type="hidden" name="_wp_http_referer" value="/wordpress_58991/contacts/"/></form></div> 
</div>  
</div>  
</div>
</div>  
</div>
</div> </div> 
<footer class="entry-footer">
</footer> 
</article> 
</main> 
</div> 
</div> 
</div> 
</div>
            
            
            
            
            
            
		<footer id="colophon" class="site-footer default container" role="contentinfo">
			<div class="footer-full-width-area-wrap">
			<div class="container">
			<section id="footer-full-width-area" class="footer-full-width-area widget-area"><aside id="durand_widget_subscribe_follow-2" class="widget widget-subscribe"><div class="subscribe-block">
			<h2 class="widget-title">Get Our special offers</h2> <h4 class="subscribe-block__message">and the ultimate hair care tips &amp; tricks!</h4>
			<form method="POST" action="#" class="subscribe-block__form"><input type="hidden" id="durand_subscribe" name="durand_subscribe" value="34f8ef18eb"><input type="hidden" name="_wp_http_referer" value="/wordpress_58991/"><div class="subscribe-block__input-group"><input class="subscribe-block__input" type="email" name="subscribe-mail" value="" placeholder="Your e-mail address"><a href="#" class="subscribe-block__submit btn">Subscribe</a></div><div class="subscribe-block__messages">
			<div class="subscribe-block__success hidden">You successfully subscribed</div>
			<div class="subscribe-block__error hidden"></div>
			</div></form>
			</div><div class="follow-block"><h2 class="widget-title">Let's Stay Connected</h2><div class="social-list social-list--widget social-list--icon"><ul id="social-list-1" class="social-list__items inline-list"><li id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12"><a href="https://www.facebook.com/TemplateMonster/"><span class="screen-reader-text">Facebook</span></a></li>
			<li id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13"><a href="https://twitter.com/TemplateMonster"><span class="screen-reader-text">Twitter</span></a></li>
			<li id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14"><a href="https://plus.google.com/+TemplateMonster"><span class="screen-reader-text">Google plus</span></a></li>
			<li id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16"><a href="https://www.linkedin.com/company/templatemonster-com"><span class="screen-reader-text">Linkedin</span></a></li>
			<li id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15"><a href="https://www.pinterest.com/templatemonster/"><span class="screen-reader-text">Pinterest</span></a></li>
			</ul></div></div></aside></section> </div>
			</div>
			<div class="footer-area-wrap invert">
			<div class="container">
			<section id="footer-area" class="footer-area widget-area row"><aside id="durand_widget_about-2" class="col-xs-12 col-sm-6 col-md-3  widget widget-about">
			<div class="widget-about__logo">
			<a class="widget-about__logo-link" href="https://ld-wp.template-help.com/wordpress_58991/">
			<img class="widget-about__logo-img" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/07/logo_about.png" alt="Durand">
			</a>
			</div>
			<div class="widget-about__tagline"></div>
			<div class="widget-about__content"><div class="tm_builder_outer_content" id="tm_builder_outer_content">
			<div class="tm_builder_inner_content tm_pb_gutters3">
			<p>Yearly we help thousands of women in New York to highlight their beauty, care about their precious hair and all in all make them feel special!</p>
			</div>
			</div></div></aside><aside id="nav_menu-4" class="col-xs-12 col-sm-6 col-md-3  widget widget_nav_menu"><h5 class="widget-title">navigation</h5><div class="menu-custom-menu-container"><ul id="menu-custom-menu" class="menu"><li id="menu-item-1162" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1162"><a href="https://ld-wp.template-help.com/wordpress_58991/about-us/">About us</a></li>
			<li id="menu-item-1066" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1066"><a href="https://ld-wp.template-help.com/wordpress_58991/services/">Services</a></li>
			<li id="menu-item-1064" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1064"><a href="https://ld-wp.template-help.com/wordpress_58991/our-staff/">Our staff</a></li>
			<li id="menu-item-1063" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1063"><a href="https://ld-wp.template-help.com/wordpress_58991/schedule/">Schedule</a></li>
			<li id="menu-item-1065" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1065"><a href="https://ld-wp.template-help.com/wordpress_58991/news/">News</a></li>
			<li id="menu-item-1618" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1618"><a href="https://ld-wp.template-help.com/wordpress_58991/contacts/">Contacts</a></li>
			</ul></div></aside><aside id="text-4" class="col-xs-12 col-sm-6 col-md-3  widget widget_text"><h5 class="widget-title">Contact Information</h5> <div class="textwidget"><p>4096 N Highland St, Arlington, VA</p>
			<p>32101, USA</p>
			<a href="mailto:ironmass@demolink.org">durand@demolink.org</a></div>
			</aside><aside id="text-5" class="col-xs-12 col-sm-6 col-md-3  widget widget_text"><h5 class="widget-title">We are open</h5> <div class="textwidget"><p>Mon-Thu: 9:30 - 21:00</p>
			<p>Fri: 6:00 - 21:00</p>
			<p>Sat: 10:00 - 15:00</p></div>
			</aside></section> </div>
			</div>
			<div class="footer-container">
			<div class="site-info container">
			<div class="site-info__mid-box">
			<div class="footer-copyright">© 2017 All rights reserved by DURAND</div> <nav id="footer-navigation" class="footer-menu" role="navigation">
			
			</ul> </nav> 
			</div>
			</div> 
			</div> 
		</footer>
	</div>
    
    
		
	<!-- Bootstrap Core JavaScript -->
	<script src="public/js/jquery.js"></script>

    
    <script src="public/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="resources/assets/nivoslider/jquery.nivo.slider.js"></script>
	<script>
		$('.carousel').carousel({
			interval: 5000 //changes the speed
		})
    </script>
	<script type="text/javascript">
		$(window).load(function() {
			$('#slider').nivoSlider();
		});
		var flag = 0;
		var flag1 = 0;
		$(".menu-toggle").click(function(){

			if(flag == 0){
				$(".main-navigation").addClass("toggled");
				flag = 1 ;
			}else{
				$(".main-navigation").removeClass("toggled");
				flag = 0 ;
			}		
		});
		$(".sub-menu-toggle").click(function(){
			if(flag == 0){
				$(this).addClass("active");
				$(this).parent().addClass("sub-menu-open");
				flag = 1 ;
			}else{
				$(this).parent().removeClass("sub-menu-open");
				$(this).removeClass("active");
				flag = 0 ;
			}			
		});
		$(".tm-pb-arrow-next").off().on("click",function(){
			 $(".tm_pb_slide").each(function(){
				var index = $(".tm_pb_slide").index(this);
				if(index == 0){
					$(this).addClass("tm-pb-moved-slide");
				}
				if(index ==1){
					$(this).addClass("tm-pb-active-slide");	
				}
				if(index ==2){
					$(this).css("z-index",1)
				}
				if($(this).is(':visible')){
					var index = $('.tm_pb_slide').index(this);
					console.log(index);
				}
				console.log(index);
			 });
			 
			
		});
	</script>
</body>
</html>