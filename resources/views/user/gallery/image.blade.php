<!DOCTYPE html>
<html lang="en">
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link href="../public/css/bootstrap.min.css" rel="stylesheet">
	<link href="../public/css/index.css" rel="stylesheet">
	<link href="../public/css/gallery.css" rel="stylesheet">
	<link href="../public/css/footer.css" rel="stylesheet">
	<link href="../public/css/full-slider.css" rel="stylesheet">
    <link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	
	<link rel="stylesheet" href="../resources/assets/nivoslider/themes/custom/custom.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="../resources/assets/nivoslider/nivo-slider.css" type="text/css" media="screen" />
	<link href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
</head>
<body>
	<div id="page" class="site">
		<header id="masthead" class="site-header centered container" role="banner">
			<div class="top-panel"> 
				<div class="top-panel__wrap container">
					<div class="top-panel__message">
						<div class="info-block">
							<i class="glyphicon glyphicon-earphone"></i> Call us today: <a href="tel:1-555-644-5566">1-555-644-5566</a>
						</div>
						<div class="info-block">
							<i class="glyphicon glyphicon-map-marker"></i> 7087 Richmond hwy, Alexandria, VA
						</div>
					</div>
					<div class="top-panel__search">
						<form role="search" method="get" class="search-form" action="">
							<label>
							<span class="screen-reader-text">Search for:</span>
							<input type="search" class="search-form__field" placeholder="Search …" value="" name="s" title="Search for:">
							</label>
							<button type="submit" class="search-form__submit btn btn-primary"><i class="material-icons glyphicon glyphicon-search"></i></button>
						</form>
					</div>
				</div>
			</div>
			<div class="header-container">
				<div class="header-container_wrap container">
					<div class="header-container__flex">
						<div class="header-container__center">
							<div class="site-branding">
								<div class="site-logo">
									<a class="site-logo__link" href="https://ld-wp.template-help.com/wordpress_58991/" rel="home">Durand</a>
								</div>
							</div>
							<nav id="site-navigation" class="main-navigation" role="navigation">
								<button class="menu-toggle" aria-controls="main-menu" aria-expanded="false">
								<i class="menu-toggle__icon fa fa-bars" aria-hidden="true"></i>
								Menu </button>
								<ul id="main-menu" class="menu">
                                    <li id="menu-item-1163" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1163"><a href="../home">Home</a>
								
								<span class="sub-menu-toggle"></span></li>
								<li id="menu-item-414" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-414">
                                <a href="../about">about us</a>
								
								<span class="sub-menu-toggle"></span></li>
								<li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="../gallery">Gallery</a>
                                    <ul class="sub-menu">
        								<li id="menu-item-755" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-755"><a href="image">Image</a></li>
        								<li id="menu-item-756" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-756"><a href="video">Video</a></li>
								    </ul>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="../services">Services</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="../blog">blog</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="../contact">Contact</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
                                <li id="menu-item-576" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-576">
                                    <a href="../login">Login</a>
    								<span class="sub-menu-toggle"></span>
                                </li>
                                
								</ul> 
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div id="content" class="site-content">
			
            
            
            
            <div id="content" class="site-content container">
				<div class="breadcrumbs"><div class="container"><div class="breadcrumbs__title"></div><div class="breadcrumbs__items">
				<div class="breadcrumbs__content">
				<div class="breadcrumbs__wrap"><div class="breadcrumbs__item"><a href="https://ld-wp.template-help.com/wordpress_58991/" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
				<div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div> <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">Gallery</span></div>
				</div>
				</div></div>
				<div class="clear"></div></div>
				</div>
				<div class="site-content_wrap container">
				<div class="row">
				<div id="primary" class="col-xs-12 col-md-12">
				<main id="main" class="site-main" role="main">
				<article id="post-412" class="post-412 page type-page status-publish hentry no-thumb">
				<header class="entry-header">
					<h1 class="entry-title screen-reader-text">Gallery</h1> 
				</header> 
				<div class="entry-content">
					<div class="row container">
						<h1>Photo Gallery</h1>
						<br>
						<!-- The container for the list of example images -->
						<div id="links">
							<a data-gallery="" title="Natalia" href="http://farm4.static.flickr.com/3741/11188919165_b73f1ddc32_b.jpg">
								<img src="http://farm4.static.flickr.com/3741/11188919165_b73f1ddc32_s.jpg">
							</a>
							<a data-gallery="" title="Delft Touch in the kitchen" href="http://farm4.static.flickr.com/3812/11188050815_5e8cccf0f6_b.jpg">
								<img src="http://farm4.static.flickr.com/3812/11188050815_5e8cccf0f6_s.jpg">
							</a>
							<a data-gallery="" title="Father Figure Project: The Social Construct" href="http://farm4.static.flickr.com/3703/11191153143_04bbbc8301_b.jpg">
								<img src="http://farm4.static.flickr.com/3703/11191153143_04bbbc8301_s.jpg">
							</a>
							<a data-gallery="" title="Higherbeams." href="http://farm4.static.flickr.com/3821/11190956343_09b8022ea2_b.jpg">
								<img src="http://farm4.static.flickr.com/3821/11190956343_09b8022ea2_s.jpg">
							</a>
							<a data-gallery="" title="Contact" href="http://farm8.static.flickr.com/7325/11186917235_003d9df5d6_b.jpg">
								<img src="http://farm8.static.flickr.com/7325/11186917235_003d9df5d6_s.jpg">
							</a>
							<a data-gallery="" title="The Track" href="http://farm6.static.flickr.com/5501/11186913445_c88dd25c59_b.jpg">
								<img src="http://farm6.static.flickr.com/5501/11186913445_c88dd25c59_s.jpg">
							</a>
							<a data-gallery="" title="TheHallsOfAdoration" href="http://farm4.static.flickr.com/3715/11186895035_8596236621_b.jpg">
								<img src="http://farm4.static.flickr.com/3715/11186895035_8596236621_s.jpg">
							</a>
							<a data-gallery="" title="Si me dijeran pide un deseo" href="http://farm8.static.flickr.com/7414/11190086653_e330723a8c_b.jpg">
								<img src="http://farm8.static.flickr.com/7414/11190086653_e330723a8c_s.jpg">
							</a>
							<a data-gallery="" title="untitled" href="http://farm3.static.flickr.com/2891/11188743634_f339e72b79_b.jpg">
								<img src="http://farm3.static.flickr.com/2891/11188743634_f339e72b79_s.jpg">
							</a>
							<a data-gallery="" title="Loch Ba" href="http://farm8.static.flickr.com/7390/11187408694_9dd7188ca8_b.jpg">
								<img src="http://farm8.static.flickr.com/7390/11187408694_9dd7188ca8_s.jpg">
							</a>
							<a data-gallery="" title="Remarkable Rocks 2" href="http://farm3.static.flickr.com/2831/11187401055_1e498bde13_b.jpg">
								<img src="http://farm3.static.flickr.com/2831/11187401055_1e498bde13_s.jpg">
							</a>
							<a data-gallery="" title="The imagination tree" href="http://farm6.static.flickr.com/5530/11188853224_91ebabac12_b.jpg">
								<img src="http://farm6.static.flickr.com/5530/11188853224_91ebabac12_s.jpg">
							</a>
							<a data-gallery="" title="Blue Patricia Lake" href="http://farm4.static.flickr.com/3720/11190734053_7cf9749bdb_b.jpg">
								<img src="http://farm4.static.flickr.com/3720/11190734053_7cf9749bdb_s.jpg">
							</a>
							<a data-gallery="" title="Goblin Brute" href="http://farm4.static.flickr.com/3740/11186937956_d8d8422169_b.jpg">
								<img src="http://farm4.static.flickr.com/3740/11186937956_d8d8422169_s.jpg">
							</a>
							<a data-gallery="" title="." href="http://farm8.static.flickr.com/7421/11191341825_b2bf635f25_b.jpg">
								<img src="http://farm8.static.flickr.com/7421/11191341825_b2bf635f25_s.jpg">
							</a>
							<a data-gallery="" title="be the light 336.365" href="http://farm3.static.flickr.com/2809/11189301843_9f41b02a47_b.jpg">
								<img src="http://farm3.static.flickr.com/2809/11189301843_9f41b02a47_s.jpg">
							</a>
							<a data-gallery="" title="Lofoten Islands" href="http://farm4.static.flickr.com/3788/11193226453_fd9b06008c_b.jpg">
								<img src="http://farm4.static.flickr.com/3788/11193226453_fd9b06008c_s.jpg">
							</a>
							<a data-gallery="" title="もみじ" href="http://farm4.static.flickr.com/3737/11189043544_e992a227d3_b.jpg">
								<img src="http://farm4.static.flickr.com/3737/11189043544_e992a227d3_s.jpg">
							</a>
							<a data-gallery="" title="Nocturna en Albarracín./ Nocturne near Albarracín (Teruel, Spain)." href="http://farm8.static.flickr.com/7455/11187819606_c008b36106_b.jpg">
								<img src="http://farm8.static.flickr.com/7455/11187819606_c008b36106_s.jpg">
							</a>
							<a data-gallery="" title="DIAMOND IN THE ROUGH" href="http://farm8.static.flickr.com/7319/11188792234_dfea30f688_b.jpg">
								<img src="http://farm8.static.flickr.com/7319/11188792234_dfea30f688_s.jpg">
							</a>
							<a data-gallery="" title="Imperfection" href="http://farm3.static.flickr.com/2817/11188666563_091e4bb4b4_b.jpg">
								<img src="http://farm3.static.flickr.com/2817/11188666563_091e4bb4b4_s.jpg">
							</a>
							<a data-gallery="" title="Late Again" href="http://farm6.static.flickr.com/5531/11186088035_98d5c7be84_b.jpg">
								<img src="http://farm6.static.flickr.com/5531/11186088035_98d5c7be84_s.jpg">
							</a>
							<a data-gallery="" title="Canyons" href="http://farm6.static.flickr.com/5502/11189048145_f234c1c4a1_b.jpg">
								<img src="http://farm6.static.flickr.com/5502/11189048145_f234c1c4a1_s.jpg">
							</a>
							<a data-gallery="" title="Brouillard sur Lanaud" href="http://farm6.static.flickr.com/5477/11193030495_dc7d99caff_b.jpg">
								<img src="http://farm6.static.flickr.com/5477/11193030495_dc7d99caff_s.jpg">
							</a>
							<a data-gallery="" title="Incontri di lavoro" href="http://farm8.static.flickr.com/7333/11189762165_7092588e1f_b.jpg">
								<img src="http://farm8.static.flickr.com/7333/11189762165_7092588e1f_s.jpg">
							</a>
							<a data-gallery="" title="Tijdens de wandeling terug van De panne naar Oostduinkerke" href="http://farm6.static.flickr.com/5542/11186573045_89102139d2_b.jpg">
								<img src="http://farm6.static.flickr.com/5542/11186573045_89102139d2_s.jpg">
							</a>
							<a data-gallery="" title="absorbed" href="http://farm4.static.flickr.com/3751/11194208465_ccfb0df6d3_b.jpg">
								<img src="http://farm4.static.flickr.com/3751/11194208465_ccfb0df6d3_s.jpg">
							</a>
							<a data-gallery="" title="Metal,  Steam and Dedication.  North Yorkshire Moors Railway." href="http://farm6.static.flickr.com/5531/11189794436_99c1e0dcfd_b.jpg">
								<img src="http://farm6.static.flickr.com/5531/11189794436_99c1e0dcfd_s.jpg">
							</a>
							<a data-gallery="" title="Tutorial tuesday" href="http://farm4.static.flickr.com/3767/11194056215_786a05dcb9_b.jpg">
								<img src="http://farm4.static.flickr.com/3767/11194056215_786a05dcb9_s.jpg">
							</a>
							<a data-gallery="" title="My Street." href="http://farm8.static.flickr.com/7375/11187509905_0366640771_b.jpg">
								<img src="http://farm8.static.flickr.com/7375/11187509905_0366640771_s.jpg">
							</a>
							<a data-gallery="" title="Histoire comme ça" href="http://farm3.static.flickr.com/2821/11187333644_25ae318b9a_b.jpg">
								<img src="http://farm3.static.flickr.com/2821/11187333644_25ae318b9a_s.jpg">
							</a>
							<a data-gallery="" title="" href="http://farm4.static.flickr.com/3797/11186343795_9e20e0a0d0_b.jpg">
								<img src="http://farm4.static.flickr.com/3797/11186343795_9e20e0a0d0_s.jpg">
							</a>
							<a data-gallery="" title="Alice" href="http://farm6.static.flickr.com/5501/11190429845_6b3bf13263_b.jpg">
								<img src="http://farm6.static.flickr.com/5501/11190429845_6b3bf13263_s.jpg">
							</a>
							<a data-gallery="" title="Irreversible" href="http://farm8.static.flickr.com/7437/11188139865_91b1fbb522_b.jpg">
								<img src="http://farm8.static.flickr.com/7437/11188139865_91b1fbb522_s.jpg">
							</a>
							<a data-gallery="" title="Kurhaus Binz" href="http://farm8.static.flickr.com/7311/11194373534_956b5e79d9_b.jpg">
								<img src="http://farm8.static.flickr.com/7311/11194373534_956b5e79d9_s.jpg">
							</a>
							<a data-gallery="" title="the lure of the easter lily" href="http://farm8.static.flickr.com/7365/11194393934_297a0bb9e2_b.jpg">
								<img src="http://farm8.static.flickr.com/7365/11194393934_297a0bb9e2_s.jpg">
							</a>
							<a data-gallery="" title="What's Inside My Bag" href="http://farm4.static.flickr.com/3780/11189972615_a62b8c7213_b.jpg">
								<img src="http://farm4.static.flickr.com/3780/11189972615_a62b8c7213_s.jpg">
							</a>
							<a data-gallery="" title="photo boillon christophe / photo macro au carré syrphe & fleur de lys / le souvenir du regard d'un insecte n°2" href="http://farm8.static.flickr.com/7385/11191819703_75990b9d9b_b.jpg">
								<img src="http://farm8.static.flickr.com/7385/11191819703_75990b9d9b_s.jpg">
							</a>
							<a data-gallery="" title="Please Don't Jump!" href="http://farm3.static.flickr.com/2859/11195652165_7a406e0d7c_b.jpg">
								<img src="http://farm3.static.flickr.com/2859/11195652165_7a406e0d7c_s.jpg">
							</a>
							<a data-gallery="" title="Pink Orchid.  Orquídea rosa." href="http://farm4.static.flickr.com/3711/11186551613_f3e986c66b_b.jpg">
								<img src="http://farm4.static.flickr.com/3711/11186551613_f3e986c66b_s.jpg">
							</a>
							<a data-gallery="" title="autumn sun rays" href="http://farm6.static.flickr.com/5471/11194154564_088959d971_b.jpg">
								<img src="http://farm6.static.flickr.com/5471/11194154564_088959d971_s.jpg">
							</a>
							<a data-gallery="" title="A Fallen Sky" href="http://farm6.static.flickr.com/5494/11193293955_df6ae68522_b.jpg">
								<img src="http://farm6.static.flickr.com/5494/11193293955_df6ae68522_s.jpg">
							</a>
							<a data-gallery="" title="the drilling has stopped" href="http://farm4.static.flickr.com/3739/11193143803_0d5dd1d935_b.jpg">
								<img src="http://farm4.static.flickr.com/3739/11193143803_0d5dd1d935_s.jpg">
							</a>
							<a data-gallery="" title="Sunset at Cayucos, CA" href="http://farm4.static.flickr.com/3785/11193020376_054ba0c2cb_b.jpg">
								<img src="http://farm4.static.flickr.com/3785/11193020376_054ba0c2cb_s.jpg">
							</a>
							<a data-gallery="" title="IMG_1124" href="http://farm4.static.flickr.com/3690/11191491034_3f1af54e5e_b.jpg">
								<img src="http://farm4.static.flickr.com/3690/11191491034_3f1af54e5e_s.jpg">
							</a>
							<a data-gallery="" title="Eiffel's Shadow Part 2" href="http://farm8.static.flickr.com/7432/11189047666_2ff11060db_b.jpg">
								<img src="http://farm8.static.flickr.com/7432/11189047666_2ff11060db_s.jpg">
							</a>
							<a data-gallery="" title="Old Time Space Pilots!" href="http://farm6.static.flickr.com/5508/11197130006_ac579704c7_b.jpg">
								<img src="http://farm6.static.flickr.com/5508/11197130006_ac579704c7_s.jpg">
							</a>
							<a data-gallery="" title="Sky reflected" href="http://farm4.static.flickr.com/3811/11191218745_ef8cf811c9_b.jpg">
								<img src="http://farm4.static.flickr.com/3811/11191218745_ef8cf811c9_s.jpg">
							</a>
							<a data-gallery="" title="Rain at sea" href="http://farm6.static.flickr.com/5528/11191191844_7de9305ff0_b.jpg">
								<img src="http://farm6.static.flickr.com/5528/11191191844_7de9305ff0_s.jpg">
							</a>
							<a data-gallery="" title="Frostig - Frosty" href="http://farm4.static.flickr.com/3827/11194862425_a1a26fce68_b.jpg">
								<img src="http://farm4.static.flickr.com/3827/11194862425_a1a26fce68_s.jpg">
							</a>
							<a data-gallery="" title="Orange Ladybird" href="http://farm6.static.flickr.com/5527/11186670076_11ee081969_b.jpg">
								<img src="http://farm6.static.flickr.com/5527/11186670076_11ee081969_s.jpg">
							</a>
							<a data-gallery="" title="Pink orchid" href="http://farm6.static.flickr.com/5514/11193598423_981cb3b900_b.jpg">
								<img src="http://farm6.static.flickr.com/5514/11193598423_981cb3b900_s.jpg">
							</a>
							<a data-gallery="" title="225.2013" href="http://farm8.static.flickr.com/7449/11193159913_76ddcd3f02_b.jpg">
								<img src="http://farm8.static.flickr.com/7449/11193159913_76ddcd3f02_s.jpg">
							</a>
							<a data-gallery="" title="DSC_7489" href="http://farm4.static.flickr.com/3834/11187370596_195c175098_b.jpg">
								<img src="http://farm4.static.flickr.com/3834/11187370596_195c175098_s.jpg">
							</a>
							<a data-gallery="" title="G-TCDC 2 A321-231S Thomas Cook Airlines UK(on del) MAN 03DEC13" href="http://farm4.static.flickr.com/3665/11192324763_729bd2eb5a_b.jpg">
								<img src="http://farm4.static.flickr.com/3665/11192324763_729bd2eb5a_s.jpg">
							</a>
							<a data-gallery="" title="cold day..but nice" href="http://farm6.static.flickr.com/5546/11191357813_754fc41bde_b.jpg">
								<img src="http://farm6.static.flickr.com/5546/11191357813_754fc41bde_s.jpg">
							</a>
							<a data-gallery="" title="Grinnell in B & W" href="http://farm8.static.flickr.com/7409/11190351243_5c9d34b844_b.jpg">
								<img src="http://farm8.static.flickr.com/7409/11190351243_5c9d34b844_s.jpg">
							</a>
							<a data-gallery="" title="15th corner of metallic shadows" href="http://farm3.static.flickr.com/2824/11192248094_5239c5bb88_b.jpg">
								<img src="http://farm3.static.flickr.com/2824/11192248094_5239c5bb88_s.jpg">
							</a>
							<a data-gallery="" title="The Receipt, Tesco Supermarket, Thornton Heath" href="http://farm3.static.flickr.com/2832/11190558394_4198ee9487_b.jpg">
								<img src="http://farm3.static.flickr.com/2832/11190558394_4198ee9487_s.jpg">
							</a>
							<a data-gallery="" title="Golmaramara, Manisa" href="http://farm8.static.flickr.com/7378/11190583213_87c55a04b1_b.jpg">
								<img src="http://farm8.static.flickr.com/7378/11190583213_87c55a04b1_s.jpg">
							</a>
							<a data-gallery="" title="IMG_4539 Alstroemeria hibrida( Peruvian lily or lily of the Incas)" href="http://farm3.static.flickr.com/2881/11190387313_07d3e4019e_b.jpg">
								<img src="http://farm3.static.flickr.com/2881/11190387313_07d3e4019e_s.jpg">
							</a>
							<a data-gallery="" title="On the Move" href="http://farm4.static.flickr.com/3710/11190198736_329108ab33_b.jpg">
								<img src="http://farm4.static.flickr.com/3710/11190198736_329108ab33_s.jpg">
							</a>
							<a data-gallery="" title="Twistleton Erratic Mystery Solved" href="http://farm4.static.flickr.com/3736/11188821145_4d7c5cba2d_b.jpg">
								<img src="http://farm4.static.flickr.com/3736/11188821145_4d7c5cba2d_s.jpg">
							</a>
							<a data-gallery="" title="Welcome To Dream Theater" href="http://farm3.static.flickr.com/2888/11194228946_a22ca1273f_b.jpg">
								<img src="http://farm3.static.flickr.com/2888/11194228946_a22ca1273f_s.jpg">
							</a>
							<a data-gallery="" title="Kingfisher 115" href="http://farm4.static.flickr.com/3761/11192804084_fc9c224b92_b.jpg">
								<img src="http://farm4.static.flickr.com/3761/11192804084_fc9c224b92_s.jpg">
							</a>
							<a data-gallery="" title="Oh Christmas Tree, Oh Christmas Tree (A7 + FE 28-70mm)" href="http://farm3.static.flickr.com/2843/11188372516_d2b638c316_b.jpg">
								<img src="http://farm3.static.flickr.com/2843/11188372516_d2b638c316_s.jpg">
							</a>
							<a data-gallery="" title="Réservoir" href="http://farm4.static.flickr.com/3772/11186008524_70203c9862_b.jpg">
								<img src="http://farm4.static.flickr.com/3772/11186008524_70203c9862_s.jpg">
							</a>
							<a data-gallery="" title="Anna's Hummingbird" href="http://farm3.static.flickr.com/2879/11193622663_2596f23ccb_b.jpg">
								<img src="http://farm3.static.flickr.com/2879/11193622663_2596f23ccb_s.jpg">
							</a>
							<a data-gallery="" title="India. Udaipur. Templo de Sas Bahu" href="http://farm4.static.flickr.com/3766/11191115443_ef304dab2d_b.jpg">
								<img src="http://farm4.static.flickr.com/3766/11191115443_ef304dab2d_s.jpg">
							</a>
							<a data-gallery="" title="Afternoon Tea Treat" href="http://farm6.static.flickr.com/5546/11186477995_5e02323a9a_b.jpg">
								<img src="http://farm6.static.flickr.com/5546/11186477995_5e02323a9a_s.jpg">
							</a>
							<a data-gallery="" title="0007559" href="http://farm4.static.flickr.com/3715/11192580616_2534046023_b.jpg">
								<img src="http://farm4.static.flickr.com/3715/11192580616_2534046023_s.jpg">
							</a>
							<a data-gallery="" title="Liège-Guillemins" href="http://farm4.static.flickr.com/3813/11191726844_ef38874aa2_b.jpg">
								<img src="http://farm4.static.flickr.com/3813/11191726844_ef38874aa2_s.jpg">
							</a>
							<a data-gallery="" title="autunno in Langa" href="http://farm8.static.flickr.com/7295/11186551094_fd7f6bb741_b.jpg">
								<img src="http://farm8.static.flickr.com/7295/11186551094_fd7f6bb741_s.jpg">
							</a>
							<a data-gallery="" title="Junco" href="http://farm4.static.flickr.com/3775/11188715846_ccaaa3c859_b.jpg">
								<img src="http://farm4.static.flickr.com/3775/11188715846_ccaaa3c859_s.jpg">
							</a>
							<a data-gallery="" title="illuminati" href="http://farm8.static.flickr.com/7390/11190619986_b01b312535_b.jpg">
								<img src="http://farm8.static.flickr.com/7390/11190619986_b01b312535_s.jpg">
							</a>
							<a data-gallery="" title="Chicago on the prairie" href="http://farm8.static.flickr.com/7444/11192183673_9b5dc14b82_b.jpg">
								<img src="http://farm8.static.flickr.com/7444/11192183673_9b5dc14b82_s.jpg">
							</a>
							<a data-gallery="" title="In the wee small hours" href="http://farm4.static.flickr.com/3690/11188858774_880657dfe5_b.jpg">
								<img src="http://farm4.static.flickr.com/3690/11188858774_880657dfe5_s.jpg">
							</a>
							<a data-gallery="" title="Sunset at Rocca Calascio" href="http://farm4.static.flickr.com/3823/11188037413_12432dbd1b_b.jpg">
								<img src="http://farm4.static.flickr.com/3823/11188037413_12432dbd1b_s.jpg">
							</a>
							<a data-gallery="" title="Autumn Woodland Walk" href="http://farm8.static.flickr.com/7445/11193295285_0e9a33b308_b.jpg">
								<img src="http://farm8.static.flickr.com/7445/11193295285_0e9a33b308_s.jpg">
							</a>
							<a data-gallery="" title="The Scoop" href="http://farm4.static.flickr.com/3704/11189390906_9f6d188f6f_b.jpg">
								<img src="http://farm4.static.flickr.com/3704/11189390906_9f6d188f6f_s.jpg">
							</a>
							<a data-gallery="" title="Night Messenger" href="http://farm3.static.flickr.com/2820/11187385286_22a1684442_b.jpg">
								<img src="http://farm3.static.flickr.com/2820/11187385286_22a1684442_s.jpg">
							</a>
							<a data-gallery="" title="IMG_0986 (1)" href="http://farm6.static.flickr.com/5540/11194162765_82ff5370ba_b.jpg">
								<img src="http://farm6.static.flickr.com/5540/11194162765_82ff5370ba_s.jpg">
							</a>
							<a data-gallery="" title="Opening in the reeds" href="http://farm8.static.flickr.com/7297/11194070595_8fbe773d36_b.jpg">
								<img src="http://farm8.static.flickr.com/7297/11194070595_8fbe773d36_s.jpg">
							</a>
							<a data-gallery="" title="坐看 ~奧入瀨溪流,  雲井之流  of Oirase Keiryu~" href="http://farm4.static.flickr.com/3809/11192250074_96d068f6b6_b.jpg">
								<img src="http://farm4.static.flickr.com/3809/11192250074_96d068f6b6_s.jpg">
							</a>
							<a data-gallery="" title="Blue and Gold" href="http://farm4.static.flickr.com/3724/11191614145_eeae8c7a2a_b.jpg">
								<img src="http://farm4.static.flickr.com/3724/11191614145_eeae8c7a2a_s.jpg">
							</a>
							<a data-gallery="" title="NYC in Silhouette" href="http://farm6.static.flickr.com/5509/11188709506_8b49202344_b.jpg">
								<img src="http://farm6.static.flickr.com/5509/11188709506_8b49202344_s.jpg">
							</a>
							<a data-gallery="" title="I hate love" href="http://farm4.static.flickr.com/3684/11187175694_f1e860e03f_b.jpg">
								<img src="http://farm4.static.flickr.com/3684/11187175694_f1e860e03f_s.jpg">
							</a>
							<a data-gallery="" title="Jambatan Seri Warasan #2" href="http://farm8.static.flickr.com/7392/11192683083_c9bcc29620_b.jpg">
								<img src="http://farm8.static.flickr.com/7392/11192683083_c9bcc29620_s.jpg">
							</a>
							<a data-gallery="" title="Flower lakeside" href="http://farm6.static.flickr.com/5517/11188543255_769549853a_b.jpg">
								<img src="http://farm6.static.flickr.com/5517/11188543255_769549853a_s.jpg">
							</a>
							<a data-gallery="" title="Gobi Desert" href="http://farm4.static.flickr.com/3756/11186900993_40b7e05620_b.jpg">
								<img src="http://farm4.static.flickr.com/3756/11186900993_40b7e05620_s.jpg">
							</a>
							<a data-gallery="" title="The Cradle of Light" href="http://farm8.static.flickr.com/7374/11191272254_b768cca1ab_b.jpg">
								<img src="http://farm8.static.flickr.com/7374/11191272254_b768cca1ab_s.jpg">
							</a>
							<a data-gallery="" title="Matched up 337/365" href="http://farm6.static.flickr.com/5519/11190498386_b0e551fcd3_b.jpg">
								<img src="http://farm6.static.flickr.com/5519/11190498386_b0e551fcd3_s.jpg">
							</a>
							<a data-gallery="" title="IMGP9696-stavrosstam" href="http://farm6.static.flickr.com/5483/11192544226_e76f7000ab_b.jpg">
								<img src="http://farm6.static.flickr.com/5483/11192544226_e76f7000ab_s.jpg">
							</a>
							<a data-gallery="" title="IMG_3117" href="http://farm4.static.flickr.com/3758/11191372883_200105f62f_b.jpg">
								<img src="http://farm4.static.flickr.com/3758/11191372883_200105f62f_s.jpg">
							</a>
							<a data-gallery="" title="A world apart..." href="http://farm3.static.flickr.com/2831/11189347715_9d8f5b400b_b.jpg">
								<img src="http://farm3.static.flickr.com/2831/11189347715_9d8f5b400b_s.jpg">
							</a>
							<a data-gallery="" title="Lights on Cape Cod Bay" href="http://farm6.static.flickr.com/5490/11187042085_cd6483e25c_b.jpg">
								<img src="http://farm6.static.flickr.com/5490/11187042085_cd6483e25c_s.jpg">
							</a>
							<a data-gallery="" title="second spring" href="http://farm4.static.flickr.com/3801/11196375183_f6abd5b25e_b.jpg">
								<img src="http://farm4.static.flickr.com/3801/11196375183_f6abd5b25e_s.jpg">
							</a>
							<a data-gallery="" title="Perfect Landing" href="http://farm6.static.flickr.com/5503/11194858816_e46fc17ecd_b.jpg">
								<img src="http://farm6.static.flickr.com/5503/11194858816_e46fc17ecd_s.jpg">
							</a>
							<a data-gallery="" title="Oh rose thou art sick..." href="http://farm4.static.flickr.com/3806/11189687433_9183bb0d19_b.jpg">
								<img src="http://farm4.static.flickr.com/3806/11189687433_9183bb0d19_s.jpg">
							</a>
							<a data-gallery="" title="" href="http://farm8.static.flickr.com/7448/11188317995_4127b61baa_b.jpg">
								<img src="http://farm4.static.flickr.com/3806/11189687433_9183bb0d19_s.jpg">
							</a>
						</div>
						<br>
					</div>
					<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
					<div id="blueimp-gallery" class="blueimp-gallery">
						<!-- The container for the modal slides -->
						<div class="slides"></div>
						<!-- Controls for the borderless lightbox -->
						<h3 class="title"></h3>
						<a class="prev">‹</a>
						<a class="next">›</a>
						<a class="close">×</a>
						<a class="play-pause"></a>
						<ol class="indicator"></ol>
						<!-- The modal dialog, which will be used to wrap the lightbox content -->
						<div class="modal fade">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" aria-hidden="true">×</button>
										<h4 class="modal-title"></h4>
									</div>
									<div class="modal-body next"></div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default pull-left prev">
											<i class="glyphicon glyphicon-chevron-left"></i>
											Previous
										</button>
										<button type="button" class="btn btn-primary next">
											Next
											<i class="glyphicon glyphicon-chevron-right"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div> 
				</div> 
				<footer class="entry-footer">
				</footer> 
				</article> 
				</main> 
				</div> 
				</div>
            
            
            
            
            
		</div>
		<footer id="colophon" class="site-footer default container" role="contentinfo">
			<div class="footer-full-width-area-wrap">
			<div class="container">
			<section id="footer-full-width-area" class="footer-full-width-area widget-area"><aside id="durand_widget_subscribe_follow-2" class="widget widget-subscribe"><div class="subscribe-block">
			<h2 class="widget-title">Get Our special offers</h2> <h4 class="subscribe-block__message">and the ultimate hair care tips &amp; tricks!</h4>
			<form method="POST" action="#" class="subscribe-block__form"><input type="hidden" id="durand_subscribe" name="durand_subscribe" value="34f8ef18eb"><input type="hidden" name="_wp_http_referer" value="/wordpress_58991/"><div class="subscribe-block__input-group"><input class="subscribe-block__input" type="email" name="subscribe-mail" value="" placeholder="Your e-mail address"><a href="#" class="subscribe-block__submit btn">Subscribe</a></div><div class="subscribe-block__messages">
			<div class="subscribe-block__success hidden">You successfully subscribed</div>
			<div class="subscribe-block__error hidden"></div>
			</div></form>
			</div><div class="follow-block"><h2 class="widget-title">Let's Stay Connected</h2><div class="social-list social-list--widget social-list--icon"><ul id="social-list-1" class="social-list__items inline-list"><li id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12"><a href="https://www.facebook.com/TemplateMonster/"><span class="screen-reader-text">Facebook</span></a></li>
			<li id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13"><a href="https://twitter.com/TemplateMonster"><span class="screen-reader-text">Twitter</span></a></li>
			<li id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14"><a href="https://plus.google.com/+TemplateMonster"><span class="screen-reader-text">Google plus</span></a></li>
			<li id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16"><a href="https://www.linkedin.com/company/templatemonster-com"><span class="screen-reader-text">Linkedin</span></a></li>
			<li id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15"><a href="https://www.pinterest.com/templatemonster/"><span class="screen-reader-text">Pinterest</span></a></li>
			</ul></div></div></aside></section> </div>
			</div>
			<div class="footer-area-wrap invert">
			<div class="container">
			<section id="footer-area" class="footer-area widget-area row"><aside id="durand_widget_about-2" class="col-xs-12 col-sm-6 col-md-3  widget widget-about">
			<div class="widget-about__logo">
			<a class="widget-about__logo-link" href="https://ld-wp.template-help.com/wordpress_58991/">
			<img class="widget-about__logo-img" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/07/logo_about.png" alt="Durand">
			</a>
			</div>
			<div class="widget-about__tagline"></div>
			<div class="widget-about__content"><div class="tm_builder_outer_content" id="tm_builder_outer_content">
			<div class="tm_builder_inner_content tm_pb_gutters3">
			<p>Yearly we help thousands of women in New York to highlight their beauty, care about their precious hair and all in all make them feel special!</p>
			</div>
			</div></div></aside><aside id="nav_menu-4" class="col-xs-12 col-sm-6 col-md-3  widget widget_nav_menu"><h5 class="widget-title">navigation</h5><div class="menu-custom-menu-container"><ul id="menu-custom-menu" class="menu"><li id="menu-item-1162" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1162"><a href="https://ld-wp.template-help.com/wordpress_58991/about-us/">About us</a></li>
			<li id="menu-item-1066" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1066"><a href="https://ld-wp.template-help.com/wordpress_58991/services/">Services</a></li>
			<li id="menu-item-1064" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1064"><a href="https://ld-wp.template-help.com/wordpress_58991/our-staff/">Our staff</a></li>
			<li id="menu-item-1063" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1063"><a href="https://ld-wp.template-help.com/wordpress_58991/schedule/">Schedule</a></li>
			<li id="menu-item-1065" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1065"><a href="https://ld-wp.template-help.com/wordpress_58991/news/">News</a></li>
			<li id="menu-item-1618" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1618"><a href="https://ld-wp.template-help.com/wordpress_58991/contacts/">Contacts</a></li>
			</ul></div></aside><aside id="text-4" class="col-xs-12 col-sm-6 col-md-3  widget widget_text"><h5 class="widget-title">Contact Information</h5> <div class="textwidget"><p>4096 N Highland St, Arlington, VA</p>
			<p>32101, USA</p>
			<a href="mailto:ironmass@demolink.org">durand@demolink.org</a></div>
			</aside><aside id="text-5" class="col-xs-12 col-sm-6 col-md-3  widget widget_text"><h5 class="widget-title">We are open</h5> <div class="textwidget"><p>Mon-Thu: 9:30 - 21:00</p>
			<p>Fri: 6:00 - 21:00</p>
			<p>Sat: 10:00 - 15:00</p></div>
			</aside></section> </div>
			</div>
			<div class="footer-container">
			<div class="site-info container">
			<div class="site-info__mid-box">
			<div class="footer-copyright">© 2017 All rights reserved by DURAND</div> <nav id="footer-navigation" class="footer-menu" role="navigation">
			
			</ul> </nav> 
			</div>
			</div> 
			</div> 
		</footer>
	</div>
		
	<!-- Bootstrap Core JavaScript -->
	<script src="../public/js/jquery.js"></script>
    

    
    <script src="../public/js/bootstrap.min.js"></script>
    <script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
	<script>
		$('.carousel').carousel({
			interval: 5000 //changes the speed
		})
    </script>
	<script type="text/javascript">
		$(window).load(function() {
			$('#slider').nivoSlider();
		});
		var flag = 0;
		var flag1 = 0;
		$(".menu-toggle").click(function(){

			if(flag == 0){
				$(".main-navigation").addClass("toggled");
				flag = 1 ;
			}else{
				$(".main-navigation").removeClass("toggled");
				flag = 0 ;
			}		
		});
		$(".sub-menu-toggle").click(function(){
			if(flag == 0){
				$(this).addClass("active");
				$(this).parent().addClass("sub-menu-open");
				flag = 1 ;
			}else{
				$(this).parent().removeClass("sub-menu-open");
				$(this).removeClass("active");
				flag = 0 ;
			}			
		});
		$(".tm-pb-arrow-next").off().on("click",function(){
			 $(".tm_pb_slide").each(function(){
				var index = $(".tm_pb_slide").index(this);
				if(index == 0){
					$(this).addClass("tm-pb-moved-slide");
				}
				if(index ==1){
					$(this).addClass("tm-pb-active-slide");	
				}
				if(index ==2){
					$(this).css("z-index",1)
				}
				if($(this).is(':visible')){
					var index = $('.tm_pb_slide').index(this);
					console.log(index);
				}
				console.log(index);
			 });
			 
			
		});
	</script>
</body>
</html>