@extends('themes.salon.index')
@section('css')

@stop
@section('js')

    
@stop
@section('content')

  <div class="breadcrumbs"><div class="container"><div class="breadcrumbs__title"></div><div class="breadcrumbs__items">
			<div class="breadcrumbs__content">
			<div class="breadcrumbs__wrap"><div class="breadcrumbs__item"><a href="https://ld-wp.template-help.com/wordpress_58991/" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
			<div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div> <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">User info</span></div>
			</div>
			</div></div><div class="clear"></div></div>
			</div>
			<div class="site-content_wrap container">
			<div class="row">
			<div id="primary" class="col-md-12 col-lg-9">
				<section>
				
					<div class="profile-head">
					<div class="col-md- col-sm-4 col-xs-12">
					<img src="http://www.newlifefamilychiropractic.net/wp-content/uploads/2014/07/300x300.gif" class="img-responsive" />
					<h6></h6>
					</div><!--col-md-4 col-sm-4 col-xs-12 close-->
					<div class="col-md-5 col-sm-5 col-xs-12">
					<h5>{{$users->name}}</h5>
					<p></p>
					<ul>
					<li><span class="glyphicon glyphicon-briefcase"></span> {{$users->birthday}}</li>
					<li><span class="glyphicon glyphicon-map-marker"></span> VN</li>
					<li><span class="glyphicon glyphicon-home"></span> {{$users->address}}</li>
					<li><span class="glyphicon glyphicon-phone"></span> <a href="#" title="call">{{$users->phone}}</a></li>
					</ul>
					</div><!--col-md-8 col-sm-8 col-xs-12 close-->




					</div><!--profile-head close-->
					<!--container close-->


					<div id="sticky">
						
						<!-- Nav tabs -->
						<ul class="nav nav-tabs nav-menu" role="tablist">
						  <li class="active">
							  <a href="#profile" role="tab" data-toggle="tab">
								  <i class="fa fa-male"></i> Profile
							  </a>
						  </li>
						  <li><a href="#change" role="tab" data-toggle="tab">
							  <i class="fa fa-key"></i> Edit Profile
							  </a>
						  </li>
                          <li><a href="logout">
							  <i class="fa fa-key"></i> LogOut
							  </a>
						  </li>
						</ul><!--nav-tabs close-->
						
						<!-- Tab panes -->
						<div class="tab-content">
						<div class="tab-pane fade active in" id="profile">
						<div class="container">
					
					<br clear="all" />
					<div class="row">
					<div class="col-md-12">
					<h4 class="pro-title">Bio Graph</h4>
					</div><!--col-md-12 close-->


					<div class="col-md-12">

					<div class="table-responsive responsiv-table">
					  <table class="table bio-table">
							<Thead>
							<tr>
							<td>Firstname</td>
							<td>: jenifer</td> 
							<td>: jenifer</td> 
							<td>: jenifer</td> 
							</tr>
							<thead>
						  <tbody>
						 <tr>      
							<td>Firstname</td>
							<td>: jenifer</td> 
							<td>: jenifer</td> 
							<td>: jenifer</td> 
						 </tr>
						 <tr>    
							<td>Lastname</td>
							<td>: smith</td>     
							<td>: jenifer</td> 
							<td>: jenifer</td> 							
						 </tr>
						 <tr>    
							<td>Birthday</td>
							<td>: 10 january 1980</td>   
							<td>: jenifer</td> 
							<td>: jenifer</td> 							
							
						</tr>
						<tr>    
							<td>Contury</td>
							<td>: U.S.A.</td>       
							<td>: jenifer</td> 
							<td>: jenifer</td> 							

						</tr>
						<tr>
							<td>Occupation</td>
							<td>: Web Designer</td> 
							<td>: jenifer</td> 
							<td>: jenifer</td> 							

						 </tr>

						</tbody>
					  </table>
					  </div><!--table-responsive close-->
					</div><!--col-md-6 close-->

					

					</div><!--row close-->




					</div><!--container close-->
					</div><!--tab-pane close-->
						  
						  
					<div class="tab-pane fade" id="change">
					<div class="container fom-main">
					<div class="row">
					<div class="col-sm-12">
					<h2 class="register">Edit Your Profile</h2>
					</div><!--col-sm-12 close-->

					</div><!--row close-->
					<br />
					<div class="row">

					<form class="form-horizontal main_form text-left" action=" " method="post"  id="contact_form">
					<fieldset>


					<div class="form-group col-md-12">
					  <label class="col-md-10 control-label">First Name</label>  
					  <div class="col-md-12 inputGroupContainer">
					  <div class="input-group">
					  <input  name="first_name" placeholder="First Name" class="form-control"  type="text">
						</div>
					  </div>
					</div>

					<!-- Text input-->

					<div class="form-group col-md-12">
					  <label class="col-md-10 control-label" >Last Name</label> 
						<div class="col-md-12 inputGroupContainer">
						<div class="input-group">
					  <input name="last_name" placeholder="Last Name" class="form-control"  type="text">
						</div>
					  </div>
					</div>

					<!-- Text input-->
						   <div class="form-group col-md-12">
					  <label class="col-md-10 control-label">E-Mail</label>  
						<div class="col-md-12 inputGroupContainer">
						<div class="input-group">
					  <input name="email" placeholder="E-Mail Address" class="form-control"  type="text">
						</div>
					  </div>
					</div>


					<!-- Text input-->
						   
					<div class="form-group col-md-12">
					  <label class="col-md-10 control-label">Phone #</label>  
						<div class="col-md-12 inputGroupContainer">
						<div class="input-group">
					  <input name="phone" placeholder="(845)555-1212" class="form-control" type="text">
						</div>
					  </div>
					</div>

					<!-- Text input-->
						  
					

					


				

					<!-- Select Basic -->
					




					


					
					<div class="form-group col-md-12">
					  <label class="col-md-10 control-label">Choose Password</label>  
					  <div class="col-md-12 inputGroupContainer">
					  <div class="input-group">
					  <input  name="first_name" placeholder="Choose Password" class="form-control"  type="password">
						</div>
					  </div>
					</div>



					<div class="form-group col-md-12">
					  <label class="col-md-10 control-label">Confiram Password</label>  
					  <div class="col-md-12 inputGroupContainer">
					  <div class="input-group">
					  <input  name="first_name" placeholder="Confiram Password" class="form-control"  type="password">
						</div>
					  </div>
					</div>


					<!-- radio checks -->
					 

					<!-- upload profile picture -->					
					<!-- Button -->
					<div class="form-group col-md-12">
					  <div class="col-md-12">
						<button type="submit" class="btn btn-warning submit-button" >Save</button>
						<button type="submit" class="btn btn-warning submit-button" >Cancel</button>

					  </div>
					</div>
					</fieldset>
					</form>
					</div><!--row close-->
					</div><!--container close -->          
					</div><!--tab-pane close-->
					</div><!--tab-content close-->
					</div><!--container close-->

					</section><!--section close-->
			</div> 
			<div id="sidebar-primary" class="col-md-12 col-lg-3 sidebar-primary widget-area" role="complementary">
				<aside id="search-2" class="widget widget_search"><h4 class="widget-title">Search</h4><form role="search" method="get" class="search-form" action="https://ld-wp.template-help.com/wordpress_58991/">
			<label>
			<span class="screen-reader-text">Search for:</span>
			<input type="search" class="search-form__field" placeholder="Search …" value="" name="s" title="Search for:">
			</label>
			<button type="submit" class="search-form__submit btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>
			</form></aside><aside id="durand_widget_subscribe_follow-3" class="widget widget-subscribe"><div class="subscribe-block">
			<h4 class="widget-title">Get Our special offers</h4> <h4 class="subscribe-block__message">and the ultimate haircare tips &amp; tricks!</h4>
			<form method="POST" action="#" class="subscribe-block__form"><input type="hidden" id="durand_subscribe" name="durand_subscribe" value="f4e2a07765"><input type="hidden" name="_wp_http_referer" value="/wordpress_58991/news/"><div class="subscribe-block__input-group"><input class="subscribe-block__input" type="email" name="subscribe-mail" value="" placeholder="Your e-mail address"><a href="#" class="subscribe-block__submit btn">Subscribe</a></div><div class="subscribe-block__messages">
			<div class="subscribe-block__success hidden">You successfully subscribed</div>
			<div class="subscribe-block__error hidden"></div>
			</div></form>
			</div><div class="follow-block"><h4 class="widget-title">Let's Stay Connected</h4><div class="social-list social-list--widget social-list--icon"><ul id="social-list-1" class="social-list__items inline-list"><li id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12"><a href="https://www.facebook.com/TemplateMonster/"><span class="screen-reader-text">Facebook</span></a></li>
			<li id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13"><a href="https://twitter.com/TemplateMonster"><span class="screen-reader-text">Twitter</span></a></li>
			<li id="menu-item-14" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-14"><a href="https://plus.google.com/+TemplateMonster"><span class="screen-reader-text">Google plus</span></a></li>
			<li id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16"><a href="https://www.linkedin.com/company/templatemonster-com"><span class="screen-reader-text">Linkedin</span></a></li>
			<li id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-15"><a href="https://www.pinterest.com/templatemonster/"><span class="screen-reader-text">Pinterest</span></a></li>
			</ul></div></div><style scoped="">#durand_widget_subscribe_follow-3 .subscribe-block{background-color:#eaeaea}#durand_widget_subscribe_follow-3 .follow-block{background-color:#dedede}</style></aside>
			
			<aside id="durand_widget_about_author-2" class="widget durand widget-about-author"><h4 class="widget-title">About Durand</h4><div class="about-author"><div class="about-author_avatar"><img class="about-author_img" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/06/image46-512x442.jpg" width="250" height="250" alt="avatar"></div><div class="about-author_content"><h4 class="about-author_name">admin</h4><div class="about-author_description">Throughout high school and college, she was famous for her talent of cutting hair and creating unique hairstyles. Shane Doe decided to convert her hobby into a profession, she was most excited to make people feel good about themselves.</div><div class="about-author_btn_box"><a href="#" class="about-author_btn btn">Read More</a></div></div>
			</div>
			</aside><aside id="categories-3" class="widget widget_categories"><h4 class="widget-title">Categories</h4><label class="screen-reader-text" for="cat">Categories</label><select name="cat" id="cat" class="postform">
			<option value="-1">Select Category</option>
			<option class="level-0" value="53">Celebrities</option>
			<option class="level-0" value="52">Colors in Trend</option>
			<option class="level-0" value="55">Events</option>
			<option class="level-0" value="51">Haircuts</option>
			<option class="level-0" value="54">Tips</option>
			<option class="level-0" value="1">Uncategorized</option>
			</select>
			
			</aside><aside id="archives-3" class="widget widget_archive"><h4 class="widget-title">Archives</h4> <label class="screen-reader-text" for="archives-dropdown-3">Archives</label>
			<select id="archives-dropdown-3" name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
			<option value="">Select Month</option>
			<option value="https://ld-wp.template-help.com/wordpress_58991/2016/05/"> May 2016 </option>
			<option value="https://ld-wp.template-help.com/wordpress_58991/2016/04/"> April 2016 </option>
			<option value="https://ld-wp.template-help.com/wordpress_58991/2016/03/"> March 2016 </option>
			<option value="https://ld-wp.template-help.com/wordpress_58991/2015/02/"> February 2015 </option>
			</select>
			</aside> <aside id="recent-posts-2" class="widget widget_recent_entries"> <h4 class="widget-title">Recent Posts</h4> <ul>
			<li>
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/05/03/how-to-wash-and-style-curly-hair/">How to wash and style curly hair</a>
			</li>
			<li>
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/24/returning-to-a-natural-hair-texture/">Returning to a natural hair texture</a>
			</li>
			<li>
			<a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/21/winterish-version-of-rainbow-hair/">Winterish version of rainbow hair?</a>
			</li>
			</ul>
			</aside>
			</div>
			</div> 
			</div> 
			</div>
      
        
@stop



			
		