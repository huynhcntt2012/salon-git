@extends('themes.salon.index')
@section('css')

@stop
@section('js')

    
@stop
@section('content')

<div id="content" class="site-content container">
<div class="breadcrumbs"><div class="container"><div class="breadcrumbs__title"></div><div class="breadcrumbs__items">
<div class="breadcrumbs__content">
<div class="breadcrumbs__wrap"><div class="breadcrumbs__item"><a href="https://ld-wp.template-help.com/wordpress_58991/" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
<div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div> <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">News</span></div>
</div>
</div></div><div class="clear"></div></div>
</div>
<div class="site-content_wrap container">
<div class="row">
<div id="primary" class="col-md-12 col-lg-9">
<main id="main" class="site-main" role="main">
<header>
<h1 class="page-title screen-reader-text">News</h1>
</header>
<div class="posts-list posts-list--default one-right-sidebar no-sidebars-before">
        @foreach($post as $post)
<article id="post-206" class="posts-list__item card post-thumbnail--fullwidth post-206 post type-post status-publish format-standard has-post-thumbnail hentry category-tips tag-celebrities tag-events has-thumb">
<div class="post-list__item-content">
<figure class="post-thumbnail">
                    <a href="#" class="post-thumbnail__link">
<a href="https://ld-wp.template-help.com/wordpress_58991/2016/05/03/how-to-wash-and-style-curly-hair/" class="post-thumbnail__link"><img class="post-thumbnail__img wp-post-image" src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image15-1280x510.jpg" alt="How to wash and style curly hair" width="1280" height="510"></a>
<div class="post__cats"><a href="https://ld-wp.template-help.com/wordpress_58991/category/tips/" rel="tag">Tips</a></div>
</figure> 
<header class="entry-header">
<h4 class="entry-title"><a href="https://ld-wp.template-help.com/wordpress_58991/2016/04/21/winterish-version-of-rainbow-hair/" rel="bookmark">Winterish version of rainbow hair?</a></h4> </header> 
</div> 
<div class="entry-meta">
<span class="post__date">
<a href="https://ld-wp.template-help.com/wordpress_58991/2016/03/30/" class="post__date-link"><time datetime="2016-03-30T12:35:24+00:00" title="2016-03-30T12:35:24+00:00">March 30</time></a> </span>
<span class="posted-by">by <a href="https://ld-wp.template-help.com/wordpress_58991/author/admin/" class="posted-by__author" rel="author">admin</a></span>
<span class="post__comments">
<a href="https://ld-wp.template-help.com/wordpress_58991/2016/03/30/image-format/#respond" class="post__comments-link">comments (0)</a> </span>
</div> 
<div class="entry-content">
</div> 
</div> 
</div> 
<footer class="entry-footer">
<a href="https://ld-wp.template-help.com/wordpress_58991/2016/03/25/video-format/" class="btn btn-primary"><span class="btn__text">Read more</span><i class="glyphicon glyphicon-arrow-right"></i></a> </footer> 
</article> 
        @endforeach
</div> 
    <!----- page -------!>
<nav class="navigation pagination" role="navigation">
<h2 class="screen-reader-text">Posts navigation</h2>
<div class="nav-links">
            <a class="next page-numbers" href="#">
                <i class="glyphicon glyphicon-chevron-left"></i>
            </a>
<span class="page-numbers current">1</span>
<a class="page-numbers" href="https://ld-wp.template-help.com/wordpress_58991/news/page/2/">2</a>
            <a class="next page-numbers" href="$">
<a class="next page-numbers" href="https://ld-wp.template-help.com/wordpress_58991/news/page/2/"><i class="glyphicon glyphicon-chevron-right"></i></a></div>
            </a>
        </div>
</nav>
</main> 
</div> 
    @include('themes.'.$arrayBase['themes'].'.menuleft')
    </div> 
</div> 
        
@stop



	