@extends('themes.salon.index')
@section('css')

@stop
@section('js')

    
@stop
@section('content')

  
<div id="content" class="site-content">


	<div class="breadcrumbs"><div class="container"><div class="breadcrumbs__title"></div><div class="breadcrumbs__items">
		<div class="breadcrumbs__content">
		<div class="breadcrumbs__wrap"><div class="breadcrumbs__item"><a href="https://ld-wp.template-help.com/wordpress_58991/" class="breadcrumbs__item-link is-home" rel="home" title="Home">Home</a></div>
		<div class="breadcrumbs__item"><div class="breadcrumbs__item-sep">/</div></div> <div class="breadcrumbs__item"><span class="breadcrumbs__item-target">About us</span></div>
		</div>
		</div></div><div class="clear"></div></div>
		</div>
		<div class="site-content_wrap">
		<div class="row">
		<div id="primary" class="col-xs-12 col-md-12">
		<main id="main" class="site-main" role="main">
		<article id="post-1118" class="post-1118 page type-page status-publish hentry no-thumb">
		<header class="entry-header">
		<h1 class="entry-title screen-reader-text">About us</h1> </header> 
		<div class="entry-content">
		<div class="tm_builder_outer_content" id="tm_builder_outer_content">
		<div class="tm_builder_inner_content tm_pb_gutters3">
		<div class="tm_pb_section  tm_pb_section_0 tm_section_regular tm_section_transparent">
		<div class="container">
		<div class=" row tm_pb_row tm_pb_row_0">
		<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_0 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_center main-title tm_pb_text_0">
		<h2 style="text-align: center;">Welcome to the <em>best</em> hairdressing salon <em>in New York!</em></h2>
		</div>  
		</div>  
		</div>  
		</div><div class="container">
		<div class=" row tm_pb_row tm_pb_row_1">
		<div class="tm_pb_column tm_pb_column_1_2  tm_pb_column_1 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
		<div class="tm_pb_module tm-waypoint tm_pb_image tm_pb_animation_off tm_pb_image_0 tm_always_center_on_mobile tm-animated">
		<img src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/06/image58.jpg" alt="">
		</div><hr class="tm_pb_module tm_pb_space tm_pb_divider tm_pb_divider_0"><div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left  tm_pb_text_1">
		<h4>The History</h4>
		<p>This salon, founded by three best friends Mary, Brenda and Alberta is now the New York’s women staple for either getting a nice and beautiful hairdo, chit chatting with their favorite stylist and just to feel special again…</p>
		<h4>The Teamwork</h4>
		<p>Everything that we do at the salon we do as a team… Despite all of our stylists being of different age, social background, and heritage, we’re all looking for just one simple goal in sight – namely, to fulfill the ever-evolving needs of New York’s charming ladies!</p>
		</div>  
		</div>  <div class="tm_pb_column tm_pb_column_1_2  tm_pb_column_2 col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
		<div class="tm_pb_module tm-waypoint tm_pb_image tm_pb_animation_off tm_pb_image_1 tm_always_center_on_mobile tm-animated">
		<img src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/06/image59.jpg" alt="">
		</div><hr class="tm_pb_module tm_pb_space tm_pb_divider tm_pb_divider_1"><div class="tm_pb_text tm_pb_module tm_pb_bg_layout_light tm_pb_text_align_left  tm_pb_text_2">
		<h4>The Wide Range of Services</h4>
		<p>Starting with offering traditional service of female, male and children’s haircuts, and all the way to Hair coloringing, hair care treatments, and special occasion hairdos, we’re eager to offer you more hairdressing services than anybody else in the city!</p>
		<h4>The Atmosphere</h4>
		<p>It’s no wonder, that our returning customers total to 85% of all of our clients, and the new people get used to our lovely atmosphere at once. Besides teas, coffees and even salads (which are free of charge) our charming stylists also always offer a nice chit-chat while they do you a hairdo!</p>
		</div>  
		</div>  
		</div>  
		</div>
		</div>  <div class="tm_pb_section  tm_pb_section_1 tm_section_regular tm_section_transparent">
		<div class="container">
		<div class=" row tm_pb_row tm_pb_row_2">
		<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_3 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<hr class="tm_pb_module tm_pb_space tm_pb_divider tm_pb_divider_2">
		</div>  
		</div>  
		</div><div class="container">
		<div class="row tm_pb_row tm_pb_row_3 tm_pb_row_4col">
		<div class="tm_pb_column tm_pb_column_1_4  tm_pb_column_4 col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
		<div class="tm_pb_circle_counter tm_pb_circle_counter_0 tm_pb_bg_layout_light tm_pb_module"><div class="tm_pb_circle_counter_bar container-width-change-notify" data-number-value="12" data-bar-bg-color="#ffffff" data-size="230" data-bar-type="round" data-color="#d28881" data-alpha="1" data-circle-width="4">
		<div class="percent">
		<p style="visibility: visible;">
		<span class="percent-value">12</span> </p>
		</div>
		<h3>Services</h3><canvas height="230" width="230"></canvas></div></div> 
		</div>  <div class="tm_pb_column tm_pb_column_1_4  tm_pb_column_5 col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
		<div class="tm_pb_circle_counter tm_pb_circle_counter_1 tm_pb_bg_layout_light tm_pb_module"><div class="tm_pb_circle_counter_bar container-width-change-notify" data-number-value="20" data-bar-bg-color="#ffffff" data-size="230" data-bar-type="round" data-color="#d28881" data-alpha="1" data-circle-width="4">
		<div class="percent">
		<p style="visibility: visible;">
		<span class="percent-value">20</span> </p>
		</div>
		<h3>Awards</h3><canvas height="230" width="230"></canvas></div></div> 
		</div>  <div class="tm_pb_column tm_pb_column_1_4  tm_pb_column_6 col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
		<div class="tm_pb_circle_counter tm_pb_circle_counter_2 tm_pb_bg_layout_light tm_pb_module"><div class="tm_pb_circle_counter_bar container-width-change-notify" data-number-value="10" data-bar-bg-color="#ffffff" data-size="230" data-bar-type="round" data-color="#d28881" data-alpha="1" data-circle-width="4">
		<div class="percent">
		<p style="visibility: visible;">
		<span class="percent-value">10</span> </p>
		</div>
		<h3>Stylists</h3><canvas height="230" width="230"></canvas></div></div> 
		</div>  <div class="tm_pb_column tm_pb_column_1_4  tm_pb_column_7 col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-3">
		<div class="tm_pb_circle_counter tm_pb_circle_counter_3 tm_pb_bg_layout_light tm_pb_module"><div class="tm_pb_circle_counter_bar container-width-change-notify" data-number-value="92" data-bar-bg-color="#ffffff" data-size="230" data-bar-type="round" data-color="#d28881" data-alpha="1" data-circle-width="4">
		<div class="percent">
		<p style="visibility: visible;">
		<span class="percent-value">92</span> </p>
		</div>
		<h3>Discount club members</h3><canvas height="230" width="230"></canvas></div></div> 
		</div>  
		</div>  
		</div><div class="container">
		<div class=" row tm_pb_row tm_pb_row_4">
		<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_8 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<hr class="tm_pb_module tm_pb_space tm_pb_divider tm_pb_divider_3">
		</div>  
		</div>  
		</div>
		</div>  <div class="tm_pb_section  tm_pb_section_2 tm_section_regular tm_section_transparent">
		<div class="container">
		<div class=" row tm_pb_row tm_pb_row_5">
		<div class="tm_pb_column tm_pb_column_4_4  tm_pb_column_9 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div id="myCarousel" class="carousel slide">
											<!-- Indicators -->

											<!-- Wrapper for Slides -->
											<div class="carousel-inner">
												<div class="item">
													<!-- Set the first background image using inline CSS below. -->
													<div class="fill" style="background-image:url('https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image8.jpg');"></div>
													
															
													
												</div>
												<div class="item">
													<!-- Set the second background image using inline CSS below. -->
													<div class="fill" style="background-image:url('https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image10.jpg');"></div>
													
												</div>
												<div class="item active">
													<!-- Set the third background image using inline CSS below. -->
													<div class="fill" style="background-image:url('https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image9.jpg');"></div>
													
												</div>
											</div>

											<!-- Controls -->
											<a class="left carousel-control custom" href="#myCarousel" data-slide="prev">
												<span class="icon-prev"></span>
											</a>
											<a class="right carousel-control custom" href="#myCarousel" data-slide="next">
												<span class="icon-next"></span>
											</a>

										</div>
										</div>
		</div>  
		</div>  
		</div>
		</div>
        <div class="tm_pb_section  tm_pb_section_3 tm_pb_with_background tm_section_regular">
		
		</div>  
		</div>
		</div> </div> 
		<footer class="entry-footer">
		</footer> 
		</article> 
		</main> 
		</div> 
		</div>
    
    
    
</div>
      
        
@stop




		