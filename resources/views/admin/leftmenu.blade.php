<li class="header">MAIN NAVIGATION</li>
        
        <li <?php if($selectedmenu == 1 && $selectedmenu == 1) echo 'class="active"'; ?>><a href="bill"><i class="fa fa-book"></i> <span>Phiếu Dịch Vụ</span></a></li>
        <li class="<?php if($selectedmenu == 2) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Quản Lý Khách Hàng</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($selectedmenu == 2 && $selecteditem == 1) echo 'class="active"'; ?>><a href="customerinfo"><i class="fa fa-circle-o"></i> Tra Cứu Thông Tin</a></li>
            <li <?php if($selectedmenu == 2 && $selecteditem == 2) echo 'class="active"'; ?>><a href="customerdate"><i class="fa fa-circle-o"></i> Tra Cứu Theo Ngày</a></li>
            <li <?php if($selectedmenu == 2 && $selecteditem == 3) echo 'class="active"'; ?>><a href="customer"><i class="fa fa-circle-o"></i> Thêm Khách Hàng</a></li>
          </ul>
        </li>
        
        <li class="<?php if($selectedmenu == 3) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Quản Lý Bảng Tin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($selectedmenu == 3 && $selecteditem == 1) echo 'class="active"'; ?>><a href="newhome"><i class="fa fa-circle-o"></i> Bảng Tin Chính</a></li>
            <li <?php if($selectedmenu == 3 && $selecteditem == 2) echo 'class="active"'; ?>><a href="{{asset('admin/homeBlog')}}"><i class="fa fa-circle-o"></i> Bảng Tin Blog</a></li>
            <li <?php if($selectedmenu == 3 && $selecteditem == 3) echo 'class="active"'; ?>><a href="newservice"><i class="fa fa-circle-o"></i> Bảng Tin Dịch Vụ</a></li>
          </ul>
        </li>
        <li>
        
        <li class="<?php if($selectedmenu == 4) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Quản Lý Nhân Viên</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($selectedmenu == 4 && $selecteditem == 1) echo 'class="active"'; ?>><a href="employeetour"><i class="fa fa-circle-o"></i> Tour Nhân Viên</a></li>
            <li <?php if($selectedmenu == 4 && $selecteditem == 2) echo 'class="active"'; ?>><a href="employee"><i class="fa fa-circle-o"></i> Danh Sách Nhân Viên</a></li>
          </ul>
        </li>
        <li>
        
        <li class="<?php if($selectedmenu == 5) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Quản Lý Dịch Vụ</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($selectedmenu == 5 && $selecteditem == 1) echo 'class="active"'; ?>><a href="servicecategory"><i class="fa fa-circle-o"></i> Danh Mục</a></li>
            <li <?php if($selectedmenu == 5 && $selecteditem == 2) echo 'class="active"' ?>><a href="servicesub"><i class="fa fa-circle-o"></i> Loại Hình</a></li>
          </ul>
        </li>
        
        <li <?php if($selectedmenu == 6 && $selecteditem == 1) echo 'class="active"'; ?>><a href="image"><i class="fa fa-book"></i> <span>Kho Ảnh</span></a></li>
        
        <li <?php if($selectedmenu == 7 && $selecteditem == 1) echo 'class="active"'; ?>><a href="video"><i class="fa fa-book"></i> <span>Kho Video</span></a></li>
        
        <li class="<?php if($selectedmenu == 8) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Chăm Sóc Khách Hàng</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($selectedmenu == 8 && $selecteditem == 1) echo 'class="active"'; ?>><a href="point"><i class="fa fa-circle-o"></i> Cách Đổi Điểm</a></li>
            <li <?php if($selectedmenu == 8 && $selecteditem == 2) echo 'class="active"'; ?>><a href="contact"><i class="fa fa-circle-o"></i> Phản Hồi Khách Hàng</a></li>
          </ul>
        </li>
        
        <li class="<?php if($selectedmenu == 9) echo 'active'; ?> treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Thống Kê</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php if($selectedmenu == 9 && $selecteditem == 1) echo 'class="active"' ?>><a href="revenue"><i class="fa fa-circle-o"></i> Doanh Thu</a></li>
            <li <?php if($selectedmenu == 9 && $selecteditem == 2) echo 'class="active"' ?>><a href="salaries"><i class="fa fa-circle-o"></i> Lương Nhân Viên</a></li>
            <li <?php if($selectedmenu == 9 && $selecteditem == 3) echo 'class="active"' ?>><a href="revenexpen"><i class="fa fa-circle-o"></i> Các Khoản Thu Chi</a></li>
            <li <?php if($selectedmenu == 9 && $selecteditem == 4) echo 'class="active"' ?>><a href="customersta"><i class="fa fa-circle-o"></i> Khách Hàng</a></li>
            <li <?php if($selectedmenu == 9 && $selecteditem == 5) echo 'class="active"' ?>><a href="servicessta"><i class="fa fa-circle-o"></i> Dịch Vụ</a></li>
          </ul>
        </li>