<style>
.container{
    margin-top:20px;
}
.image-preview-input {
    position: relative;
	overflow: hidden;
	margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
</style>


<section class="content">
      <div class="row">
        <div class="col-md-12">
          
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Thêm Khách Hàng
              </h3>
                <?php echo $arrayBase['message']; ?>
                <form method="POST" enctype="multipart/form-data" action="customer" >
                <div class="form-group">
                  <label for="title">Tên Khách Hàng</label>
                  <input type="text" class="form-control" id="name" name="name" placeholder="name">
                </div>
                <div class="form-group">
                  <label for="title">Số Điện Thoại</label>
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="phone">
                </div>
                <div class="form-group">
                  <label for="title">Ngày Sinh</label>
                  <input type="date" class="form-control" id="birth" name="birth" placeholder="birthday">
                </div>
                
                <div class="form-group">
                  <label for="title">Mật Khẩu</label>
                  <input type="password" class="form-control" id="pass" name="pass" placeholder="password">
                </div>
                <div class="form-group">
                  <label for="title">Nhập lại mật khẩu</label>
                  <input type="password" class="form-control" id="conform" name="conform" placeholder="password">
                </div>
                
              <!-- tools box -->
              <div class="pull-right box-tools">
                
              </div>
              <div class="col-xs-12 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">  
            <!-- image-preview-filename input [CUT FROM HERE]-->
            <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename" id="img1" name="img1" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input id="img" name="img"  type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                    </div>
                </span>
            </div><!-- /input-group image-preview [TO HERE]--> 
        </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
                <textarea id="text-content" name="text-content" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              
            </div>
        
            <div class="box-footer">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary"><i></i> Send</button>
              </div>
            </div>
            </form>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>