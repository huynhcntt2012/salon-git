<style>
.container{
    margin-top:20px;
}
.image-preview-input {
    position: relative;
	overflow: hidden;
	margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
</style>


<section class="content">
      <div class="row">
        <div class="col-md-12">
          
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Bài Viết Mới
              </h3>
              <div class="form-group">
                </div>
                <form method="POST" enctype="multipart/form-data" action="posts" >
                <?php foreach ($arrayBase['data'] as $key => $value){ ?>
                <div class="form-group">
                  <label for="title">Tiêu Đề</label>
                  <input type="text" value="<?php echo $value->post_title ?>" class="form-control" id="title" name="title" placeholder="title">
                </div>
                <input type="text" id="type_action" name="type_action" value="true" hidden="true" >
                <input type="text" id="id" name="id" value="<?php echo $value->ID  ?>" hidden="true" >
                
              <!-- tools box -->
              <div class="input-group" style="width:25%; height:25%">
                <img src="../resources/views/themes/salon/public/img/<?php echo $value->post_image ?>"  />
              </div>
              <div class="input-group">
                
              </div>
              <div class="col-xs-12 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">  
            <!-- image-preview-filename input [CUT FROM HERE]-->
            <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename" id="img1" name="img1" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input id="img" name="img"  type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                    </div>
                </span>
            </div><!-- /input-group image-preview [TO HERE]--> 
        </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
                <textarea id="text-content" name="text-content" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                    <?php echo $value->post_content ?>
                </textarea>
              
            </div>
        
            <div class="box-footer">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary"><i></i> Send</button>
              </div>
            </div>
            <?php } ?>
            </form>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>