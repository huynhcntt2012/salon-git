@extends('admin.main')
@section('css')
    <link href="{{asset('/public/asset/css/mainblog.css')}}" rel="stylesheet" type="text/css"/>
   
@stop
@section('js')
    <script src="{{asset('/public/asset/js/mainblog.js')}}" type="text/javascript"></script>    
    
@stop
@section('content')

<div class="row">   
        <div class="col-lg-9">
            <h2>Create blog</h2>
            <form action="{{url('createBlogPost')}}" enctype="multipart/form-data" method="POST">
                <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" value='{{$post[0]->post_title or ""}}' name="title" id="title">
                </div>
                <div class="form-group">
                    <label for="title">Content</label>
                     <textarea id="text-content" name="content"  class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$post[0]->post_content or ""}}</textarea>         
                </div>
                <div class="checkbox">
                    <?php 
                        if(isset($post[0]->post_status) && $post[0]->post_status  =='home'){
                            echo '<label><input id="getHome" name="getHome" checked type="checkbox">Get home</label>';
                        }  else {
                            echo '<label><input id="getHome" name="getHome" type="checkbox">Get home</label>';
                        }
                    ?>                   
                </div>
                <input type="hidden" name ="name-img" id="name-img" value='{{$post[0]->post_image or ""}}'  />
                <input type="hidden" name ="post_id" id="post_id" value='{{$post[0]->ID or ""}}' />
                <button type="submit" class="btn btn-primary">save</button>
            </form>
        </div>
        <div  class="col-lg-3">
            <h2>Image represent</h2>
            <div class="box-img-rep">
                <img id="img_represent" src="{{asset('/public/images/'.(isset($post[0]->post_image)?$post[0]->post_image:'noimage.jpg').'')}}" class="img-responsive" alt="Cinque Terre">
            </div>            
            <br>
            <button class="btn btn-default" data-toggle="modal" data-target="#myModal">Set Image</button>
        </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Library image <span class="status-upload">Upload Fail</span></h4>    
           
      </div>
        <div class="modal-body" >
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Tải ảnh lên</a></li>
            <li><a data-toggle="tab" href="#menu1" onclick="getImgaes()" >Hình ảnh</a></li>
            
          </ul>

          <div class="tab-content" style="margin-top: 20px;position:relative;">
            <input type="hidden" id="name-img-upload"/>
            <div id="home" class="tab-pane fade in active" >
                
                <div class="row">
                    <div class="col-lg-12">                       
                            <div class="row">
                                <div id="load_screen">
                                    <div id="loading">

                                    </div>
                                </div>
                                <div class="col-md-12" style="text-align: center">
                                        <input type="file" id="img"/>
                                        <button id="upload" class="btn btn-primary" onclick="upload()" >Upload</button>
                                </div>
                            </div>                        
                    </div>
                </div>
            </div>
              <div id="menu1" class="tab-pane fade" >
                <div class="row" id="box-imgs">
                    <div class="col-lg-3 box-img">
                        <img src="{{asset('/public/images/1488256509.jpg')}}" class="img-responsive" alt="Cinque Terre">
                        <div class="checkbox_img">
                            <input type="radio" name="img_check" class="checkbox"/>
                        </div>
                    </div>
                    <div class="col-lg-3 box-img">
                        <img src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image15-540x510.jpg" class="img-responsive" alt="Cinque Terre">
                        <div class="checkbox_img">
                            <input type="radio" name="img_check" class="checkbox"/>
                        </div>
                    </div>
                    <div class="col-lg-3 box-img">
                        <img src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image15-540x510.jpg" class="img-responsive" alt="Cinque Terre">
                        <div class="checkbox_img">
                            <input type="radio" name="img_check" class="checkbox"/>
                        </div>
                    </div>
                    <div class="col-lg-3 box-img">
                        <img src="https://ld-wp.template-help.com/wordpress_58991/wp-content/uploads/2016/05/image15-540x510.jpg" class="img-responsive" alt="Cinque Terre">
                        <div class="checkbox_img">                      
                            <input type="radio" name="img_check" class="checkbox"/>			
                        </div>
                    </div>
                </div>
            </div>          
          </div>         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="setImgS()">Save changes</button>
      </div>
    </div>
  </div>
</div>
@stop