@extends('admin.main')

@section('css')
@endsection

@section('content')
	<section id="service" class="content">
		<div class="row">
			<div class="col-md-5">
				<div class="box">
					<div class="box-header with-border">
						<div class="col-xs-5">
							<h2>Danh mục</h2>
						</div>
						<div class="col-xs-7">
							<button type="button" 
								class="btn bg-maroon btn-flat margin pull-right" 
								data-toggle="modal" 
								data-target="#add-category"
							>
								Thêm danh mục
							</button>
						</div>
					</div>
					<div class="box-body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>STT</th>
									<th>Tên </th>
									<th>Chức năng</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="category in categories">
									<td>@{{ $index + 1 }}</td>
									<td>@{{ category.name }}</td>
									<td>
										<div class="btn-group">
											<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												<i class="fa fa-cogs"></i>
											</button>
											<ul class="dropdown-menu pull-right" role="menu">
												<li><a data-toggle="modal" data-target="#info-category">Xem chi tiết</a></li>
												<li><a data-toggle="modal" data-target="#add-category">Chỉnh sửa</a></li>
												<li><a href="#">Xóa</a></li>
											</ul>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col-md-7">
				<div class="box">
					<div class="box-header with-border">
						<div class="col-xs-4">
							<div class="form-group">
								<h2>Dịch vụ</h2>
								<label>Danh mục:</label>
								<select class="form-control">
									<option value="">Tất cả</option>
									<option>option 1</option>
									<option>option 2</option>
									<option>option 3</option>
									<option>option 4</option>
									<option>option 5</option>
								</select>
							</div>
						</div>
						<div class="col-xs-8">
							<button type="button" class="btn bg-maroon btn-flat margin pull-right">Thêm dịch vụ</button>
						</div>
					</div>
					<div class="box-body">
						<table class="table table-striped">
						<thead>
							<tr>
								<th>STT</th>
								<th>Tên </th>
								<th>Hoa hồng</th>
								<th>Gía</th>
								<th>Chức năng</th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="sub in sub_categories">
								<td>@{{ $index + 1 }}</td>
								<td>@{{ sub.name }}</td>
								<td>@{{ sub.income }}</td>
								<td>@{{ sub.price | number }}</td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<i class="fa fa-cogs"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a data-toggle="modal" data-target="#info-category">Xem chi tiết</a></li>
											<li><a data-toggle="modal" data-target="#add-category">Chỉnh sửa</a></li>
											<li><a href="#">Xóa</a></li>
										</ul>
									</div>
								</td>
							</tr>
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal modal-info fade" id="info-category" tabindex="-1" role="dialog" >
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Info Modal</h4>
				</div>
				<div class="modal-body">
					<p>One fine body&hellip;</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-outline">Save changes</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="add-category" tabindex="-1" role="dialog" >
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">Thêm</h4>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-horizontal">
								<div class="form-group">
									<label for="name-category" class="col-sm-4 control-label">Tên</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="name-category" placeholder="Email">
									</div>
				                </div>
				                <div class="form-group">
									<label for="pice" class="col-sm-4 control-label">Gía</label>
									<div class="col-sm-8">
										<input type="number" class="form-control" id="pice" placeholder="">
									</div>
				                </div>
				                <div class="form-group">
									<label for="discount" class="col-sm-4 control-label">Hoa hồng</label>
									<div class="col-sm-8">
										<input type="number" class="form-control" id="discount" placeholder="">
									</div>
				                </div>
				                <div class="form-group">
									<label for="discount" class="col-sm-4 control-label">Thông tin dịch vụ</label>
									<div class="col-sm-8">
										<textarea class="form-control" rows="3" placeholder="Nhập ..."></textarea>
									</div>
				                </div>
				           	</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Thoát</button>
					<button type="button" class="btn btn-primary">Thêm</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script src="{{asset('/public/dist/js/service.js')}}"></script>
@endsection
