<?php 
namespace App\Models;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

  
class ArrayBase{
    
    const OPTION = 'option';
    
    const URL = 'url';
    
    const THEMES = 'themes';
    
    
    public function __construct(){
        
    }
    
    
    public static function getArray($array){
        $array = array(
                        ArrayBase::OPTION => $option,
                        ArrayBase::URL => $url,
                        ArrayBase::THEMES => $themes
                        );
             
        return $array;
    }
    
}
?>