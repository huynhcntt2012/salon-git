<?php 
namespace App\Models;
  
use Illuminate\Database\Eloquent\Model;
  
class Product extends Model{
    
    const TABLE_PRODUCT = 'product';
    
    const PRODUCT_ID = 'id';
    
    const PRODUCT_NAME = 'name';
    
    const PRODUCT_DIS = 'dis';
    
    const PRODUCT_IMG = 'img';
    
    const PRODUCT_PRINCE = 'prince';
    
    const PRODUCT_HOME = 'GetHome';
	
	const PRODUCT_Id_Cus = 'Id_Cus';
	
	const PRODUCT_sub = 'sub';
    
    private $id = 'id';
    
    private $name = 'name';
    
    private $dis = 'dis';
    
    private $img = 'img';
    
    private $prince = 'prince';
    
    private $home = 'GetHome';
	
	private $Id_Cus = 'Id_Cus';
	
	private $sub = 'sub';
    
    public function __construct(){
        
    }
    
    public function setId($id){
        $this->id = $id;
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function setName($name){
        $this->name = $name;
    }
    
    public function getName(){
        return $this->name;
    }
    
}
?>