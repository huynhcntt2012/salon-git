<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//chrome.exe --user-data-dir="C:/Chrome dev session" --disable-web-security



/* ------------------------------ Route User ------------------------------ */

Route::get('/', 'HomeController@index');

Route::get('/home', 'HomeController@index');

Route::get('/about', 'AboutusController@index');

Route::get('/services', 'ServicesController@index');

Route::get('/blog', 'BlogController@index');

Route::get('/contact', 'ContactController@index');

Route::get('/gallery', 'GalleryController@index');

Route::get('/gallery/image', 'GalleryController@image');

Route::get('/gallery/video', 'GalleryController@video');

Route::get('/login', 'LoginController@index');

Route::post('/login', 'LoginController@loginUser');

Route::get('/user', 'UserController@index');

Route::get('/register', 'RegisterController@index');

Route::post('/register', 'RegisterController@process');

Route::get('/userinfor', 'RegisterController@user');

Route::get('/logout', 'LoginController@logoutUser');

/* ------------------------------ End Route USer ------------------------------ */


/* ------------------------------ Route Admin ------------------------------ */


Route::get('/admin', 'LoginController@admin');

Route::post('/admin', 'LoginController@login');

Route::get('/admin/main', 'MainController@index');

/* ---------------------------------- Main Page -------------------------------------------*/

    Route::get('/admin/logout', 'LoginController@logout');
    
    Route::get('/admin/bill', 'MainBillController@bill');
    
    Route::get('/admin/customerinfo', 'MainCustomerController@customerinfo');
    
    Route::get('/admin/customerdate', 'MainCustomerController@customerdate');
    
    Route::get('/admin/customer', 'MainCustomerController@customer');
    
    Route::post('/admin/customer', 'MainCustomerController@customer');
    
    Route::get('/admin/newhome', 'MainNewController@newhome');
    
    Route::get('/admin/createnew', 'MainNewController@createnewhome');
    
    Route::get('/admin/edithome', 'MainNewController@edithome');
    
    Route::post('/admin/posts', 'MainNewController@post');
    
    Route::get('/admin/newblog', 'MainNewController@newblog');
    
    Route::get('/admin/newservice', 'MainNewController@newservice');
    
    Route::get('/admin/employeetour', 'MainEmployeeController@employeetour');
    
    Route::get('/admin/employee', 'MainEmployeeController@employee');
    
    Route::get('/admin/servicecategory', 'MainServicesController@servicecategory');
    
    Route::get('/admin/servicesub', 'MainServicesController@servicesub');
    
    Route::get('/admin/image', 'MainImageController@image');
    
    Route::get('/admin/video', 'MainVideoController@video');
    
    Route::get('/admin/point', 'MainCareController@point');
    
    Route::get('/admin/contact', 'MainCareController@contact');
    
    Route::get('/admin/revenue', 'MainExportController@revenue');
    
    Route::get('/admin/salaries', 'MainExportController@salaries');
    
    Route::get('/admin/revenexpen', 'MainExportController@revenexpen');
    
    Route::get('/admin/customersta', 'MainExportController@customersta');
    
    Route::get('/admin/servicessta', 'MainExportController@servicessta');

/* ---------------------------------- End Main Page -------------------------------------------*/


/* ------------------------------ End Route Admin ------------------------------ */

    Route::get('/admin/homeBlog/{keyword?}/{status?}','BlogController@blogHome');
    
    Route::get('/admin/createblog/{bien?}','BlogController@createBlog');
    
     Route::get('/admin/deleteblog/{bien?}','BlogController@deleteblog');
    
    Route::post('image-upload','BlogController@imageUploadPost');
    
    Route::get('getImages','BlogController@getImages');
    
    Route::post('createBlogPost','BlogController@createBlogPost');
    
    Route::post('searchBlogPost','BlogController@searchBlogPost');
    
/* ------------------------------------- Api ----------------------------------------- */
/* ----- Category -------- */
/* ------------ Load --------- */
Route::get('api/getCategory','ApiController@getCategory');

/* ------------End Load --------- */

/* ------------ Create --------- */
Route::post('api/createCategory','ApiController@createCategory');

/* ------------End Create --------- */

/* ------------ Edit --------- */
Route::post('api/editCategory','ApiController@editCategory');

/* ------------End Edit --------- */

/* ------------ Del --------- */
Route::post('api/delCategory','ApiController@delCategory');

/* ------------End Del --------- */



/* ----- Sub -------- */
/* ------------ Load --------- */
Route::get('api/getSub','ApiController@getSub');
/* ------------End Load --------- */

/* ------------ Create --------- */
Route::post('api/createSub','ApiController@createSub');
/* ------------End Create --------- */

/* ------------ Edit --------- */
Route::post('api/editSub','ApiController@editSub');
/* ------------End Edit --------- */

/* ------------ Del --------- */
Route::post('api/delSub','ApiController@delSub');
/* ------------End Del --------- */

/* ------------------------------------- End Api ----------------------------------------- */
