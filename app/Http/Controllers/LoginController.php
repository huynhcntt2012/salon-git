<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\SessionController;
use App\Http\Database\themes;


class LoginController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $nameThemes = themes::getThemesPresent();
        
        $array = array('themes'=> $nameThemes,'url' =>'login' ,'message' =>'');
        
        
        
        if(SessionController::checkAdmin('userinfor') == true){
            $array = array('themes'=> $nameThemes,'url' =>'userinfor');
            $user = DB::table('customer')->where(array('phone' => Session::get('phone')))->first();
            return Redirect::to('userinfor')->with('arrayBase',$array)
                                                           ->with('users',$user);
            
        }
        
        return view('themes/'.themes::getThemesPresent().'/user/login')->with('arrayBase',$array);
	}
    
    public function loginUser(Request $request)
	{
        
        
        $nameThemes = themes::getThemesPresent();
        $arraywhere = array(
                            'phone' => $request->input('user_login'),
                            'password' => md5($request->input('user_pass'))
                            );
        $count = DB::table('customer')->where($arraywhere)->get();
        
        if(count($count) > 0){
            Session::put('userinfor','true');
            Session::put('phone',$request->input('user_login'));
        }
        $array = array('themes'=> $nameThemes,'url' =>'login','message' => $request->input('user_login'));
        
        if(SessionController::checkAdmin('userinfor') == false){
            
            return view('themes/'.themes::getThemesPresent().'/user/login')->with('arrayBase',$array);
        }
        $array = array('themes'=> $nameThemes,'url' =>'userinfor','message' =>'Đăng Nhập Thành Công');
        $user = DB::table('customer')->where(array('phone' => Session::get('phone')))->first();
        return view('themes/'.$nameThemes.'/user/userinfo')->with('arrayBase',$array)
                                                           ->with('users',$user);

	}
    
    public function logoutUser(Request $request)
	{
        
        $options = "Post";
        
        Session::remove('userinfor');
        
        if(SessionController::checkAdmin('userinfor') == false){
            
            return Redirect::to('home');
        }
        $array = array('themes'=> $nameThemes,'url' =>'userinfor');
        
        return view('themes/'.themes::getThemesPresent().'/user/home')->with('arrayBase',$array);
	}
    
    
    public function login(Request $request)
	{
        //Session::put('keyAdmin','true');
        $arraywhere = array(
                            'username'=>$request->input('username'),
                            'password' => md5($request->input('password'))
                            );
        $count = DB::table('admin')->where($arraywhere)->get();
        
        if(count($count) > 0){
            Session::put('keyAdmin','true');
            Session::put('userAdmin',$request->input('username'));
        }
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            
            return view('admin/login');
        }
        return Redirect::to('admin/main');
	}
    
    public function logout()
	{
        $options = "Post";
        
        Session::remove('keyAdmin');
        Session::remove('userAdmin');
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            
            return Redirect::to('admin');
        }
        
        return view('admin/main')->with('options',$options);
        
	}
    
    public function admin()
	{
        $options = "Get";
        
        if(SessionController::checkAdmin('keyAdmin') == true){
            
            return Redirect::to('admin/main');
        }

        return view('admin/login')->with('options',$options);
	}
    
}