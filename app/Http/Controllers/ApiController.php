<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use App\Http\Database\themes;
class ApiController extends Controller {
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
     /* ----------------------- Category ------------------------------- */
	public function getCategory()
	{
        $list = DB::table('category')->get();
        return json_encode($list);
	}
    /*
    paramenter
    numerical,name,status,note
    */
    public function createCategory(Request $request)
	{
        $numerical = $request->input('numerical');
        $name = $request->input('name');
        $status = $request->input('status');
        $note = $request->input('note');
        $whereInsert = array(
                              'numerical'=>$numerical,
                              'name'=>$name,
                              'status'=>$status,
                              'note'=> $note );
        
        DB::table('category')->insert($whereInsert);
        
        
        $list = DB::table('category')->get();
        return json_encode($list);
	}
    /*
    paramenter
    id,numerical,name,status,note
    */
    public function editCategory(Request $request)
	{
        $id = $request->input('id');
        $numerical = $request->input('numerical');
        $name = $request->input('name');
        $status = $request->input('status');
        $note = $request->input('note');
        $whereUpdate = array( 
                              'numerical'=>$numerical,
                              'name'=>$name,
                              'status'=>$status,
                              'note'=> $note );
        
        DB::table('category')->update($whereUpdate);
        
        
        $list = DB::table('category')->get();
        return json_encode($list);
	}
    /*
    paramenter
    id
    */
    public function delCategory(Request $request)
	{
	    $id = $request->input('id');
        DB::table('category')->where(array('id' => $id))->delete();
        $list = DB::table('category')->get();
        return json_encode($list);
	}
    
    /* -----------------------End Category ------------------------------- */
    
    
    /* ----------------------- Sub ------------------------------- */
    
    public function getSub()
	{
        
        $list = DB::table('sub')->get();
        return json_encode($list);
	}
    /*
    paramenter
    category,name,price,income,note
    */
    public function createSub(Request $request)
	{
	    $category = $request->input('category');
        $name = $request->input('name');
        $price = $request->input('price');
        $income = $request->input('income');
        $note = $request->input('note');
        $whereInsert = array(
                              'category'=>$category,
                              'name'=>$name,
                              'price'=>$price,
                              'income'=> $income,
                              'note' => $note);
        
        DB::table('sub')->insert($whereInsert);
        $list = DB::table('sub')->get();
        return json_encode($list);
	}
    /*
    paramenter
    id,category,name,price,income,note
    */
    public function editSub(Request $request)
	{  
        $id = $request->input('id');
	    $category = $request->input('category');
        $name = $request->input('name');
        $price = $request->input('price');
        $income = $request->input('income');
        $note = $request->input('note');
        $whereUpdate = array(
                              'category'=>$category,
                              'name'=>$name,
                              'price'=>$price,
                              'income'=> $income,
                              'note' => $note);
        
        DB::table('sub')->update($whereUpdate);
        $list = DB::table('sub')->get();
        return json_encode($list);
	}
    
    /*
    paramenter
    id
    */
    public function delSub(Request $request)
	{
	   
        $id = $request->input('id');
        DB::table('sub')->where(array('id' => $id))->delete();
        $list = DB::table('sub')->get();
        return json_encode($list);
	}
    
    /* ----------------------- End Sub ------------------------------- */















}





?>