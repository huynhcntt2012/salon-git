<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;
use App\Http\Database\themes;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Support\JsonableInterface;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;



class BlogController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $nameThemes = themes::getThemesPresent();
        
        $array = array('themes'=> $nameThemes,'url' =>'blog');
        
        return view('themes/'.themes::getThemesPresent().'/user/blog')->with('arrayBase',$array);
	}
        
        public function blogHome($keyword=null,$status=null){
        $title = "Bảng Blog Chính";

        $selecteditem = 2;
        $selectedmenu = 3;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        $keyword=Input::get('keyword');
        $status =Input::get('status');
        $posts= DB::table('posts')->where("post_title","LIKE","%".$keyword."%");
                                  
        if($status != null){
            $posts = $posts->where("post_status",$status);
        }  
        if($status != null && $keyword != null){
            $posts= $posts->where("post_title","LIKE","%".$keyword."%")->where("post_status",$status);
        }
        $posts= $posts->orderBy('ID','DESC')->paginate(10);
        return view('admin.pages.blogHome',['posts' => $posts])->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('keyword',$keyword)
                                    ->with('status',$status)
                                    ->with('lastPage',$posts->lastPage()) 
                                    ->with('currentPage',$posts->currentPage());
	}
        public function createBlog($bien = null){
            
            $title = "Bảng Blog Chính";
            if(SessionController::checkAdmin('keyAdmin') == false){
                            return Redirect::to('admin');
            }
            $selecteditem = 2;
            $selectedmenu = 3;
            $data = DB::table('posts')->where(array('ID' => $bien))->get();
            if($bien==null){
                return view('admin.pages.createBlog')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('post',$data);   
            }  else {
                
                if(count($data)>0){
                    return view('admin.pages.createBlog')->with('title',$title)
                                        ->with('selecteditem',$selecteditem)
                                        ->with('selectedmenu',$selectedmenu)
                                        ->with('post',$data);
                }else{
                    return view('admin.pages.error')->with('title',$title)
                                        ->with('selecteditem',$selecteditem)
                                        ->with('selectedmenu',$selectedmenu);                         
                    }
                }
            
        }
        public function getImages(){
            $data = DB::table('images')->select()->get();
            return json_encode($data);
        }
        public function deleteblog($bien){
            DB::table('posts')->where('ID',$bien)->delete();
            return back();
        }
        public function createBlogPost(Request $request){
            
            $title = "Bảng Tin Chính";

            $selecteditem = 2;
            $selectedmenu = 3;
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            
            $id = $request->input('post_id');
            $today = date("Y-m-d H:m:s");
            $title_post = $request->input('title');
            $content = $request->input('content');
            $img = $request->input('name-img');
            $post_status = $request->input('getHome');
            $status=true;
            if($post_status=='on'){
                $post_status = "home";
            }  else {
                $post_status = "blog";
            }
            try{
                if($id!=""){
                DB::table('posts')
                ->where('ID',$id)
                ->update(['updated_at' => $today,
                    'post_content' =>$content,
                    'post_title' => $title_post,
                    'post_image' => $img,
                    'post_status' => $post_status]);
                }  else {
                    DB::table('posts')->insert([
                        ['post_date' => $today,
                        'post_content' =>$content,
                        'post_title' => $title_post,
                        'post_image' => $img,
                        'post_status' => $post_status],
                    ]);
                }
            }  catch (Exception $e){
                $status = false;
            }           
            $data = array(['post_id'=>$id,
                    'post_date' => $today,
                    'post_content' =>$content,
                    'post_title' => $title_post,
                    'post_image' => $img,
                    'post_status' => $post_status],
                    'status' => $status
                    );
            /**
            
             * 
             */
            return back()->with("post",$data);
                                    
                                   
        }
        public function imageUploadPost(Request $request)
        {
            /**
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
           
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $imageName);
             *  **/
             $imageName ="";
             $status =1;
            if($request->hasFile('file')){
                $imageName = time().'.'.$request->file('file')->getClientOriginalExtension();
                $request->file('file')->move(public_path('images'), $imageName);
                DB::table('images')->insert([
                    ['name' => $imageName],
                ]);
                $status = 0;          
            }  else {
                $status =1;
            }
             $array =array('status'=>$status,'imageName'=>$imageName);
             return json_encode($array);
           
        }
        
}