<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductShip;

class OtherController extends Controller {
    
    public function login(Request $request){
        $a = DB::table('admin')->where("username","=",$request->input("username"))->where("password","=",md5($request->input("password")))->get();
        echo json_encode(count($a));
    }
    public function delcmt(Request $request){
        $id = $request->input("productid");
        $rls = $request->input("productrls"); 
        $array = array(
                        "Idproduct"=>$id,
                        "Idproduct_rls"=>$rls
                        );
        DB::table("detailrelationship")->where("Idproduct","=",$id)->where("Idproduct_rls","=",$rls)->delete();
        
        $a =array();
		$listSub =DB::table(ProductShip::TABLE_PRODUCTSHIP)
                ->where("Idproduct",'=',$id)
                ->select("detailrelationship.Idproduct_rls")
                ->get();
		foreach($listSub as $key=>$value){
			  array_push($a,$value->Idproduct_rls);
		}
		return DB::table(Product::TABLE_PRODUCT)->whereNotIn('id',$a)->get();
    }
    public function addcmt(Request $request){
        $id = $request->input("productid");
        $rls = $request->input("productrls"); 
        $array = array(
                        "Idproduct"=>$id,
                        "Idproduct_rls"=>$rls
                        );
        DB::table("detailrelationship")->insert($array);
       $list = DB::table("detailrelationship")
            ->join("product","detailrelationship.Idproduct_rls","=",'product.id')
            ->where("detailrelationship.Idproduct",'=',$id)
            ->select("product.*")
            ->get();
        //$list = DB::table("detailrelationship")->select()->get();
		echo json_encode($list);
    }
	public function getOrder(){
		$list= DB::table("order")->get();
		echo json_encode($list);
	}
	public function getOrderByDetail($id){
		$list =DB::table("order")
					->join("orderdetail","order.id","=",'orderdetail.orderdetail')
					->join("account","order.username","=","account.username")
					->where("order.id","=",$id)
					->select("orderdetail.*")
					->get();
		
		echo json_encode($list);
	}
	public function getOrderByuername($id){
		$user =DB::table("order")					
					->join("account","order.username","=","account.username")
					->where("order.id","=",$id)
					->groupBy('account.username')
					->select("account.*")
					->get();
		
		echo json_encode($user);
	}
	public function getUser(){
		$user =DB::table("account")->get();			
		echo json_encode($user);
	}
	public function getUserByUsername($username){
		$user =DB::table("account")->where("account.username","=",$username)->get();			
		echo json_encode($user);
	}
	public function UpdateStatus(Request $request){
		$username = $request->input("username");
        $status = $request->input("status"); 		
		 $user = DB::table('account')
            ->where('username',"=",$username)
            ->update(['status' => $status]);
		echo json_encode($user);
	}
    
    public function setTime(Request $request){
		$paramenu = $request->input("paramenu");
        $keymenu = $request->input("keymenu");
        
        $paratime = $request->input("paratime");
        $keytime = $request->input("keytime");
        
        
        $id = $request->input("id");
        
        $paratime = explode("*",$paratime);
        $keytime = explode("*",$keytime);
        $paramenu = explode("*",$paramenu);
        $keymenu = explode("*",$keymenu);
        $id = explode("*",$id);
        
        $array = array();
        $arraytime = array();
        for($i=0;$i<(count($paratime)-1);$i++)
        {
            $arraytime[$keytime[$i]] = $paratime[$i];
        }
        for($i=0;$i<(count($paramenu)-1);$i++)
        {
            $array[$keymenu[$i]] = $paramenu[$i];
        }
        
        for($i=0;$i<(count($id)-1);$i++)
        {
            $checkcate = DB::table("producted")->where("producted.product","=",$id[$i])->get();
            if(count($checkcate)>0){
                
                DB::table("producted")->where("product","=",$id[$i])->update($array);
            }else{
                $array['product'] = $id[$i];
                DB::table("producted")->insert($array);
            }
            $checktime = DB::table("time")->where("time.product","=",$id[$i])->get();
            if(count($checktime)>0){
                DB::table("time")->where("product","=",$id[$i])->update($arraytime);
            }else{
                $arraytime['product'] = $id[$i];
                DB::table("time")->insert($arraytime);
            }
            
            //DB::table("time")->insert($arraytime);
        }
		 
        echo json_encode($arraytime);
	}
    
}