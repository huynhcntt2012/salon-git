<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use App\Models\ArrayBase;
use App\Http\Database\themes;



class HomeController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
	   
        $nameThemes = themes::getThemesPresent();
        
        $data = DB::table('posts')->where(array('post_status' => 'home'))->get();
        
        $array = array('themes'=> $nameThemes,'url' =>'home','data' =>$data);
        
        return view('themes/'.themes::getThemesPresent().'/user/home')->with('arrayBase',$array);
	}
    
    
}