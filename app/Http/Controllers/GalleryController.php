<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use App\Http\Database\themes;

class GalleryController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
        
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        
        $nameThemes = themes::getThemesPresent();
        
        $array = array('themes'=> $nameThemes,'url' =>'gallery');
        
        return view('themes/'.$nameThemes.'/index')->with('arrayBase',$array);
	}
    
    public function image()
	{
        $nameThemes = themes::getThemesPresent();
        
        $array = array('themes'=> $nameThemes,'url' =>'gallery/image');
        
        return view('themes/'.$nameThemes.'/user/gallery/image')->with('arrayBase',$array);
	}
    
    public function video()
	{
	   
        $nameThemes = themes::getThemesPresent();
        
        $array = array('themes'=> $nameThemes,'url' =>'gallery/video');
        
        return view('themes/'.$nameThemes.'/user/gallery/video')->with('arrayBase',$array);
	}
}