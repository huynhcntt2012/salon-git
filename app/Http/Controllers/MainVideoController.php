<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\SessionController;


class MainVideoController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
    
    public function video()
	{
        $title = "Kho Video";

        $selecteditem = 1;
        $selectedmenu = 7;
        
        if(SessionController::checkAdmin('keyAdmin') == false){
            return Redirect::to('admin');
        }
        
        $array = array('url' =>'video');
        
        return view('admin/main')->with('title',$title)
                                    ->with('selecteditem',$selecteditem)
                                    ->with('selectedmenu',$selectedmenu)
                                    ->with('arrayBase', $array);
	}
    
    
}