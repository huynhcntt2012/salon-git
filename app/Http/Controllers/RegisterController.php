<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\SessionController;
use App\Http\Database\themes;

class RegisterController extends Controller {
    
    /**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $nameThemes = themes::getThemesPresent();
        
        $array = array('themes'=> $nameThemes,'url' =>'register','message' => '');

        return view('themes/'.$nameThemes.'/user/register')->with('arrayBase',$array);
	}
    
    
    
    public function process(Request $request)
	{
        $nameThemes = themes::getThemesPresent();
        
        $arraywhere = array(
                            'phone' => $request->input('phone')
                            );
        $count = DB::table('customer')->where($arraywhere)->get();
        
        if(count($count) == 0){
            $today = date("Y-m-d H:m:s");
            Session::put('userinfo','true');
            Session::put('phone',$request->input('phone'));
            $arrayinsert = array(
                                'name' => $request->input('user_name'),
                                'phone' => $request->input('phone'),
                                'birthday' => $request->input('birthday'),
                                'address' => $request->input('address'),
                                'password' => md5($request->input('pass1')),
                                'created_at' => $today);
            DB::table('customer')->insert($arrayinsert);
        }
        //Session::put('userinfo','true');
        if(SessionController::checkAdmin('userinfor') == false){
            
            $array = array('themes'=> $nameThemes,'url' =>'register','message' => 'Đăng Ký Thất Bại');
            return view('themes/'.$nameThemes.'/user/login')->with('arrayBase',$array);
        }
        return Redirect::to('userinfor');
        
	}
    
    public function user()
	{
        $nameThemes = themes::getThemesPresent();
        
        if(SessionController::checkAdmin('userinfor') == false){
            return Redirect::to('home');
            
        }
        $user = DB::table('customer')->where(array('phone' => Session::get('phone')))->first();
        $array = array('themes'=> $nameThemes,'url' =>'userinfor','message' => '','session' => Session::get('phone'));
        return view('themes/'.$nameThemes.'/user/userinfo')->with('arrayBase',$array)
                                                           ->with('users',$user);
	}
    
    
    
}