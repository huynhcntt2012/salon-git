Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
Vue.filter('number', function(value) {
	return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
})
new Vue({
	el: '#service',
	data: {
		categories: [],
		sub_categories: [],
	},
	ready: function() {
		this.getCategory();
		this.getSubCategory();
	},
	methods: {
		getCategory: function() {
			this.$http.get('/api/getCategory')
				.then(function(response) {
					// console.log(response.data);
					Vue.set(this,'categories',response.data)
				})
				.catch(function(error) {
					console.log(error);
				})
		},	
		getSubCategory: function() {
			this.$http.get('/api/getSub')
				.then(function(response) {
					console.log(response);
					Vue.set(this, 'sub_categories', response.data);
				})
				.catch(function(error) {
					console.log(error);
				})
		}	
	}
})